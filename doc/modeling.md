# Modeling applications

- Chromium site investigations
- RDX site investigation
- Basin-scale hydrogeological framework investigations

The Chromium and RDX site investigations focus on the characterization of the origin, fate, and transport of the contaminants below the ground surface.
The model analyses are three-dimensional and account for the flow in the vadose zone and regional aquifer.

Basin-scale investigations target conceptualization of the hydrogeology, hydrodynamic conditions, aquifer recharge, aquifer discharge, groundwater-supply pumping, general structure of the groundwater flow including the shape of the regional water table, etc.

# Modeling tasks for Chromium and RDX site investigations

- Evaluate distribution of contaminant mass in the subsurface
- Estimate locations of contamination source at the ground surface
- Characterize contaminant pathways in the subsurface
- Estimate locations of contaminant arrival at the top of the regional aquifer (breakthrough locations)
- Characterize governing physical and biogeochemical processes affecting contaminant transport under:
  - natural (pre-contamination) conditions
  - contamination conditions
  - remediation conditions
  - post-remediation conditions
- Identify optimal locations for site wells (pumping, extraction, monitoring)
- Guide field and laboratory activities
- Predict impact of remedial alternatives on the contaminant plume
- Optimize performance monitoring

All these modeling tasks include uncertainty, sensitivity and decision analyses.

In addition, Machine Learning methods are applied for analyses of the site hydro-bio-geochemical data to identify groundwater types, contaminant sources, and contaminant mixtures.
This type of Machine Learning (ML) analyses are also known as Blind Source Separation (BSS).

# Modeling tools

All codes and software tools applied for data- and model-based analyses are presented in a [code summary](https://gitlab.com/lanl/waffle2017/blob/master/doc/code_summary.md) section.

# Models

A general description of the site models is presented in the following sections:

- [W17](https://gitlab.com/lanl/waffle2017/blob/master/doc/waffle2017.md): large-scale three-dimensional model of the regional aquifer at the Cr site; the model domain captures the entire Pajarito Plateau
- [W12](https://gitlab.com/lanl/waffle2017/blob/master/doc/waffle2012.md): small-scale three-dimensional model of the regional aquifer at the Cr site; the model domain incorporates only a portion of the aquifer near the Cr site
- [Cr3VA](https://gitlab.com/lanl/waffle2017/blob/master/doc/Cr3VA.md): three-dimensional model of the vadose zone and the regional aquifer at the Cr site
- [RDX3VA](https://gitlab.com/lanl/waffle2017/blob/master/doc/RDX3VA.md): three-dimensional model of the vadose zone and the regional aquifer at the RDX site

# Modeling workflows

- [Database management](https://gitlab.com/lanl/waffle2017/blob/master/doc/mysql.md)
- Model calibration
- Sensitivity analysis
- Uncertainty quantification
- Well siting
- Evaluation of Intermediate Measures
- Decision Analysis
- Blind Source Separation
