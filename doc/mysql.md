LANL site database
===================

LANL site database is using MySQL (http://mysql.com).
The database provides all the inputs required for the site model analyses.
There are several data streams that are merged into the LANL site database.
 
Barometric pressure data 
------------------------
Barometric pressure data are available at http://weather.lanl.gov.
The data are collected at the LANL TA-54 weather station.
The data are provided as monthly data feeds by email (WXMach@lanl.gov).
The data are processed using ZEM scripts.
The following fields in the MySQL table `TA54BaroData` are updated:
```
+-------------+
| time        |
| tempC       |
| pressmb     |
+-------------+
```

Groundwater level data
----------------------
All the groundwater level data collected at the intermediate and regional monitoring wells at the LANL site are stored in the LANL site database.
The data are provided in the form MS Access database (complied by Shannon Allen).
The data are processed using ZEM scripts.
The following fields in the MySQL table `GroundWaterLevelData` are updated:
```
+-----------------+
| id              |
| wellname        |
| time            |
| pressurepsia    |
| temperatureC    |
| atmosphericpsia |
| piezometricWLft |
| MPHTft          |
| MDTWft          |
| probeno         |
| SNTransducer    |
| portdesc        |
| portdepthft     |
| WLmethodcode    |
| WLtypecode      |
| comments        |
| dataqualcode    |
| validreasoncode |
| sourcefile      |
| rkey            |
+-----------------+
```
The following fields in the MySQL table `ScreenData` are updated:
```
+----------------------+
| SCREEN_ID            |
| ULI                  |
| WELL_NAME            |
| CONST_SET_SEQ        |
| SCREEN_COMMON_NAME   |
| SCRN_TYPE_CODE       |
| SCREEN_MAT_CODE      |
| OPEN_TOP_DEPTH       |
| OPEN_BOTTOM_DEPTH    |
| DEPTH_UOM            |
| screen_length_ft     |
| INNER_DIAM           |
| OUTER_DIAM           |
| DIAM_UOM             |
| SLOT_SIZE            |
| SLOT_SIZE_UOM        |
| OPEN_AREA_PER_FT     |
| AREA_UOM             |
| JOINT_TYPE_CODE      |
| INSTALL_DATE         |
| DATA_SOURCE          |
| COMMENTS             |
| SOURCE_ORG_CODE      |
| DATA_STEWARD_CODE    |
| RELEASE_FLAG         |
| WEB_FLAG             |
| CREATED_BY           |
| LOAD_DATE            |
| MODIFIED_BY          |
| MODIFIED_DATE        |
| HYDRO_ZONE_TYPE_CODE |
| GEOLOGIC_UNIT_CODE   |
+----------------------+
```
The following fields in the MySQL table `PortScrn` are updated:
```
+----------------------+
| LOCATION_NAME        |
| SURFACE_ELEVATION    |
| ULI                  |
| CONST_SET_SEQ        |
| SCREEN_COMMON_NAME   |
| SCREEN_ID            |
| PORT_ID              |
| PORT_TYPE_CODE       |
| PORT_COMMON_NAME     |
| DEPTH                |
| OPEN_TOP_DEPTH       |
| OPEN_BOTTOM_DEPTH    |
| DEPTH_UOM            |
| COMMENTS             |
| SOURCE_ORG_CODE      |
| DATA_STEWARD_CODE    |
| RELEASE_FLAG         |
| WEB_FLAG             |
| CREATED_BY           |
| LOAD_DATE            |
| MODIFIED_BY          |
| MODIFIED_DATE        |
| INACTIVE_DATE        |
| HYDRO_ZONE_TYPE_CODE |
| GEOLOGIC_UNIT_CODE   |
+----------------------+
```
 
Geochemical data
-----------------
All the geochemical data collected at the intermediate and regional monitoring wells at the LANL site are stored in the LANL site database.
The data are provided in the form monthly data updates on ftp://ftp.locustec.com/ (complied by Sean Sandborgh and Nita Patel).
The data are processed using ZEM scripts.
The following fields in the MySQL table `AnalyticalData` are updated:
```
+------------------------------------------+
| ALL_DATA_LAST_MODIFIED_DATE              |
| CANYON                                   |
| LOCATION_ID                              |
| LOCATION_ID_ALIAS                        |
| NORTHING                                 |
| EASTING                                  |
| GROUND_ELEVATION                         |
| PARAMETER_CATEGORY                       |
| ANALYTICAL_METHOD_CATEGORY               |
| ANALYTICAL_METHOD                        |
| PARAMETER_CODE                           |
| PARAMETER_NAME                           |
| SAMPLING_PLAN_ID                         |
| SAMPLING_PLAN_NAME                       |
| WORK_ORDER_NO                            |
| CHAIN_OF_CUSTODY_NO                      |
| FIELD_SAMPLE_ID                          |
| SAMPLE_NAME                              |
| FIELD_PREPARATION_CODE                   |
| SAMPLE_TYPE                              |
| SAMPLE_PURPOSE                           |
| ANALYSIS_TYPE_CODE                       |
| DETECT_FLAG                              |
| REPORT_RESULT                            |
| REPORT_UNITS                             |
| LAB_QUALIFIER                            |
| VALIDATION_QUALIFIER                     |
| VALIDATION_REASON_CODES                  |
| LAB_ID                                   |
| REPORT_UNCERTAINTY                       |
| UNCERTAINTY_TYPE_CODE                    |
| REPORT_MINIMUM_DETECTABLE_ACTIVITY       |
| REPORT_METHOD_DETECTION_LIMIT            |
| REPORT_DETECTION_LIMIT                   |
| SAMPLING_METHOD_CODE                     |
| DISPLAY_SAMPLE_DATE                      |
| SAMPLE_TIME                              |
| END_SAMPLE_DATE                          |
| DISPLAY_END_SAMPLE_DATE                  |
| END_SAMPLE_TIME                          |
| SAMPLE_DELIVERY_GROUP                    |
| LAB_SAMPLE_ID                            |
| SAMPLE_RETRIEVAL_TIME                    |
| SAMPLE_RETRIEVAL_DATE                    |
| DISPLAY_SAMPLE_RETRIEVAL_DATE            |
| LAB_RECEIPT_DATE                         |
| DISPLAY_LAB_RECEIPT_DATE                 |
| PREP_TIME                                |
| PREP_DATE                                |
| DISPLAY_PREP_DATE                        |
| ANALYSIS_DATE                            |
| DISPLAY_ANALYSIS_DATE                    |
| ANALYSIS_TIME                            |
| LAB_MATRIX                               |
| DILUTION_FACTOR                          |
| PERCENT_MOISTURE                         |
| VALIDATION_STATUS_CODE                   |
| SAMPLE_RESULT_COMMENTS                   |
| RESULT_CONFIDENTIAL_FLAG                 |
| SAMPLE_DATE                              |
| RESULT_CONFIDENTIAL_RELEASE_DATE         |
| DISPLAY_RESULT_CONFIDENTIAL_RELEASE_DATE |
| CONFIDENTIAL_FLAG                        |
| UPLOAD_DATE                              |
| DISPLAY_UPLOAD_DATE                      |
| FSR_LAST_MODIFIED_DATE                   |
| DISPLAY_LAST_MODIFIED_DATE               |
| VALIDATION_DATE                          |
| DISPLAY_VALIDATION_DATE                  |
| HYDROSTRATIGRAPHIC_UNIT                  |
| GEOLOGICAL_UNIT_CODE                     |
| WELL_CLASS                               |
| SAMPLE_USAGE_CODE                        |
| EXCAVATED_FLAG                           |
| COMPOSITE_TYPE_CODE                      |
| LAB_RESULT                               |
| LAB_UNITS                                |
| UNCERTAINTY                              |
| MINIMUM_DETECTABLE_ACTIVITY              |
| METHOD_DETECTION_LIMIT                   |
| LAB_DETECTION_LIMIT                      |
| SAMPLING_COMPANY                         |
| VINTAGE_CODE                             |
| CANYON_SORT_ORDER                        |
| LOCATION_SORT_ORDER                      |
| SCREEN_COMMON_NAME                       |
| SAMPLE_START_DEPTH                       |
| SAMPLE_END_DEPTH                         |
| SAMPLE_DEPTH_UNITS                       |
| SCREEN_START_DEPTH                       |
| SCREEN_END_DEPTH                         |
| SCREEN_DEPTH_UNITS                       |
| FIELD_SAMPLE_RESULT_RECNO                |
| URI                                      |
| LOCATION_RECNO                           |
| ULI                                      |
| FIELD_SAMPLE_RECNO                       |
| USI                                      |
| BEST_VALUE_FLAG                          |
| BEST_VALUE_STATUS_CODE                   |
| BACKGROUND_COMPARISON_MEDIA_CODE         |
| USE_FLAG                                 |
| WEB_PUBLISH_DATE                         |
+------------------------------------------+
```
The following fields in the MySQL table `FieldMeasurement` are updated:
```
+----------------------------+
| LOCATION_ID                |
| FIELD_MEASUREMENT_DATE     |
| FIELD_PARAMETER            |
| FIELD_MEASUREMENT_VALUE    |
| FIELD_MEASUREMENT_UNITS    |
| FIELD_MEASUREMENT_COMMENTS |
| FIELD_MEASUREMENT_RECNO    |
| FIELD_MEASUREMENT_TIME     |
+----------------------------+
```
 
Los Alamos County water-supply pumping 
-----------       
All the Los Alamos County water-supply pumping records are stored in the LANL site database.
The data are provided by email in MS Excel format (complied by Wayne Witten).
The data are processed using ZEM scripts.
The following fields in the MySQL table `ProdDaily` are updated:
```
+-------------------+
| id                |
| time              |
| wellname          |
| runtimehr         |
| kwh               |
| production1000gal |
| productiongal     |
| gpm               |
| wlft              |
| tempF             |
| source            |
| comment           |
| dataqualcode      |
+-------------------+
```
 
LANL site pumping          
----------------- 
All the pumping records associated with field work conducted at the LANL site are stored in the LANL site database.
The data are provided by email in MS Excel format (complied by LANL staff).
The data are processed using ZEM scripts.
The following fields in the MySQL table `PumpTest` are updated:               
```
+-------------+
| id          |
| wellname    |
| pumprategpm |
| ULI         |
| starttime   |
| endtime     |
+-------------+
```