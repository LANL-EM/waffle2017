Code summary
--------------

The execution of the Chromium site model requires following codes:

- Julia: http://julialang.org
- MADS: http://mads.lanl.gov
- FEHM: http://fehm.lanl.gov
- PFLOTRAN: http://pflotran.org
- CHROTRAN: http://chrotran.lanl.gov (website coming soon)
- LaGriT: http://lagrit.lanl.gov
- MySQL: http://mysql.com
- ZEM: http://zem.lanl.gov (website coming soon)

These codes

The Chromium site model can be executed on PC, MAC and LINUX machines.
The model can be executed on a laptop or high-performance computing (HPC) clusters.
However, due to high-computational demands, the model is typically executed in parallel on the LANL computational servers (using from 64 up to 10,000 processors).
A single forward model run takes about 8 hours in a serial mode.
An inverse model run takes about a week (depending on many factors) utilizing 640 processors.

Julia
-----
Julia is a state-of-the-art, fast, high-level, high-performance dynamic programming language for technical computing (http://julialang.org).
Julia has tremendous advantages over dynamic languages such as Python and R.
Julia is orders of magnitude faster than Python and R; it is also having speed comparable with FOTRAN and C.
Julia is easy to install on any computer (PC/MAC/LINUX) as well.

MADS
----
MADS (Model Analysis & Decision Support) is also LANL-developed open-source code (http://mads.lanl.gov).
MADS is designed to perform various types of model analyses including uncertainty quantification, sensitivity analyses, machine learning, model selection, decision analyses.
The code is open source and everybody can use it.
The source code is available on github (https://github.com/madsjulia).
A user manual and references to the publications presenting the theory behind the MADS algorithms are publicly available on-line at http://mads.lanl.gov and http://madsjulia.github.io/Mads.jl.
A simple Decision Analyses demo can be found at http://madsjulia.github.io/Mads.jl/Examples/bigdt/source_termination.
MADS is easy to install on any computer (PC/MAC/LINUX; laptops/HPC clusters/cloud frameworks).
MADS is written in Julia.

Unit-testing and code-coverage analyses are performed periodically to improve code testing, verification and validation.
New unit tests are regulatory added with addition of new features.
New unit tests are also implemented when bugs are found.
Unit tests are automatically executed with each commit into the MADS master branch using a cloud service (https://travis-ci.org/madsjulia); the unit test results are publicly available.
Code coverage analyses are also performed automatically with each commit to the master branch using a cloud service (https://coveralls.io/github/madsjulia); the coverage results are also publicly available.

All the MADS unit tests can be executed through the Mads command `Mads.test()` under Julia REPL.
`Mads.test()`` executes all the unit test and performs comparisons against a "gold standard" results stored in "gold standard" files.

FEHM
----
FEHM is LANL developed groundwater flow and transport simulator.
The FEHM executable are freely distributed.
To get the FEHM executable follow instructions at http://fehm.lanl.gov/)
There are FEHM versions for PC/MAC/LINUX.

PFLOTRAN
--------
PFLOTRAN is an open source, state-of-the-art massively parallel groundwater flow and reactive transport simulator (http://pflotran.org).
PFLOTRAN solves a system of generally nonlinear partial differential equations describing multiphase, multicomponent and multiscale reactive flow and transport in geologic materials.
The code is designed to run on massively parallel computing architectures as well as workstations and laptops.
Parallelization is achieved through domain decomposition using  PETSc (Portable Extensible Toolkit for Scientific Computation; https://www.mcs.anl.gov/petsc).

CHROTRAN
--------
CHROTRAN is a novel unique biogeochemical simulator capable to represent the full three-dimensional dynamics of biochemical processes during in situ biochemical remediation of heavy metals in heterogeneous aquifers.
CHROTRAN accounts for spatial and temporal transients of (1) the heavy metal to be remediated, (2) introduced amendments (e.g., through well injection), and (3) biomass growth and decay.
The introduced amendments can include (1) an electron donor (e.g., molasses), (2) a nontoxic conservative bio-inhibitor (e.g., ethanol), and (3) a biocide (e.g., dithionite).
In addition, direct abiotic reduction by donor-metal interaction as well as donor-driven biomass growth and bio-reduction are modeled in CHROTRAN. Other critical processes, such as donor sorption, bio-fouling and biomass death are also modeled.
CHROTRAN can simulates the groundwater flow dynamics introduced due a series of injection and extraction wells.
CHROTRAN heterogeneous flow fields, arbitrarily many chemical species, a series of injection and extraction wells, and features full coupling between groundwater flow and reactive transport.
CHROTRAN is based on the existing PFLOTRAN (Lichtner et al., 2015) code framework, taking advantage of the reaction interface allowing for implementation of complex model features and leverage other facets of PFLOTRAN, such as multi-component geochemistry and high-performance computing.
CHROTRAN website is coming soon at http://chrotran.lanl.gov.

LaGriT
------
LaGriT (Los Alamos Grid Toolbox) is a library of tools that provide computational grid generation and optimization.
LaGriT can be applied for a variety of geology and geophysics modeling applications including porous flow and transport model construction, finite element modeling of stress/strain in crustal fault systems, seismology, discrete fracture networks, and hydrothermal systems.
More information can be found at http://lagrit.lanl.gov

MySQL
-----
MySQL is an open-source relational database management system using Structured Query Language (SQL).
MySQL works on many system platforms and widely used in various applications.
The LANL site database applied is build using MySQL.
The LANL site database provides all the inputs required for the site model analyses.
More information about MySQL can be found at http://mysql.com.

ZEM
---
ZEM is an computational framework for environmental management.
ZEM has been applied to perform all the regional aquifer model analyses for the LANL Chromium site.
The ZEM framework provides on-the-fly and automated integration of the site data with the site models (FEHM/PFLOTRAN/CHROTRAN) and the model-analyses tools (MADS).
For example, all the input files for FEHM and MADS needed to execute the Chromium model are automatically created using ZEM.
ZEM gets all the data from the LANL site database (water levels, concentrations, pumping rates, etc.) and places the data automatically in the FEHM and MADS input files to perform the simulations and model analyses.
However, once the input files for FEHM and MADS are build, ZEM is not needed to run the Chromium model.
ZEM is still in development and currently it is not available as an open-source code (coming soon).
ZEM website is also coming soon at http://zem.lanl.gov.
