Data Package: LANL site model `waffle2017`
==========================================

The LANL site model model `waffle2017` (below defined as W17 for short) is designed to represent the groundwater flow and contaminant transport in the regional aquifer at the the LANL site.
W17 is three-dimensional with a spatial model domain encompassing the entire Pajarito Plateau.
The top of the model is defined by the regional aquifer water table.

W7 is build through model inversion.
The model properties are estimated by calibration against observed field data.
Prior information about the model properties are provided in the form of prior information about plausible parameter ranges.
However, the these ranges are defined to be wide enough to capture existing uncertainties.

W17 accounts for transients in the municipal water-supply pumping occurring on Pajarito Plateau.
W17 also simulates the pumping at the chromium site during long-term field tests (e.g. the pumping at R-28, R-42, CrEX-1 and CrEX-3).
The drawdown responses at site monitoring wells to the water-supply and site pumping are a key piece of information allowing characterization of aquifer heterogeneity at the chromium site during the model inversion.

W17 also simulates the chromium transport.
W17 estimates the location and shape of the contamination sources as a part of the inverse process.
W17 also estimates groundwater infiltration flux and contaminant concentration at the source as well.
The other important information allowing characterization of aquifer heterogeneity at the chromium site is the chromium concentrations observed at the site monitoring wells.
The chromium concentrations provide also information about the transport properties of the regional aquifer.

In summary, W17 is calibrated to reproduce:

- regional aquifer water levels
- hydraulic drawdowns in the regional aquifer caused by water-supply and site pumping
- chromium concentration transients observed in the regional aquifer.

W17 model parameters estimated in this process are:

- aquifer permeability (accounting for 3D anisotropy and heterogeneity)
- aquifer storativity (accounting for 3D heterogeneity)
- aquifer transport properties (advective transport porosity, dispersivities)
- characteristics of contamination sources (location, size, strength, etc.)
- 
This model is released under GPL3 license.

Model setup
-----------
The model contains a number of parameters:

- kxN: hydraulic conductivity in the x coordinate direction of the Nth pilot point
- kyN: hydraulic conductivity in the y coordinate direction of the Nth pilot point
- kzN: hydraulic conductivity in the z coordinate direction of the Nth pilot point
- sN: specific storage of the Nth pilot point
- easthead/westhead: boundary conditions describing the hydraulic head on the east and west boundaries, respectively
- isN: infiltration rate for the Nth infiltration zone
- sNx0: x coordinate of the Nth infiltration zone
- sNy0: y coordinate of the Nth infiltration zone
- sNrx: radius along the x direction of the Nth infiltration zone
- sNry: radius along the y direction of the Nth infiltration zone
- sNcorr: a parameter that rotates the Nth (elliptical) infiltration zone
- logsNc: base 10 logarithm of the Chromium concentrations (ppb) in the Nth infiltration zone
- t0sN: time at which the Nth infiltration zone becomes contaminated with chromium
- krige_sigma: standard deviation used for kriging the pilot point fields
- krige_scale: scale parameter used for kriging the pilot point fields

Model execution
--------------
Executing the model requires an installation of [FEHM](https://fehm.lanl.gov/) and [Mads](https://github.com/madsjulia/Mads.jl).
The model can be run within [Julia](https://julialang.org/):

``` julia
md = Mads.loadmadsfile("w01-v01.mads")#load desired mads file
result = Mads.forward(md)
```

where `w01-v01.mads` is a MADS input file.

Processing model outputs
------------------------
Automated scripting tools are available that perform a "sanity check" on the model, producing plots of concentrations, drawdowns, and water levels as well as making movies visualizing the plume.
These scripts can be run by editing `mads/sanitycheck.jl` to point to the desired MADS input file, and executing from within [Julia](https://julialang.org/):

``` julia
include("sanitycheck.jl")
```

This will produce a set of figures in the `figs` subdirectory of the directory containing the MADS input file, a series of snapshots of the plume configuration in the `movie` subdirectory of the directory containing the mads file, and movies of the plume evolution in the `movie` subdirectory as well.

Example model outputs
---------------------
The model contains a larger number (~180,000) of outputs.
These can be broadly broken down into four categories.

- Steady-state water levels: these are described in the MADS input file as "wl_wellname_screennumber." For example, wl_r45_2 describes the water level in well R-45, screen #2.
- Transient drawdowns: these are described in the MADS input file as "PN_observationwellname_screennumber_pumpingwellname." For example, P40693_r43_1_pm04 describes the drawdown at R-43 screen 1 on day 40693 (using the way Microsoft Excel counts days) caused by pumping at PM-4.
- Chromium concentrations: these are described in the MADS input file as conc_wellname_screenname_year. For example, conc_r11_2011 describes the concentration at R-11 in 2011.
- Chromium trends at a well: these are described in the MADS input file as conc_wellname_screennumber_slope. For example, conc_r50_1_slope describes the rate at which Chromium is increasing in ppb/year.

Directory structure:
-------

All the files associated with W17 are stored in the following directory structure:

```
   |-data
   |-mads
   |---anew01
   |-----w01-v04_restart
   |---anew01a
   |---anew02
   |-----w01_restart
   |---anew03
   |---anew04
   |---anew05
   |---anew06
   |---cond03
   |---cond04
   |---cond05
   |---cond05a
   |---cond06
   |---cond07
   |---cond08
   |---cond09
   |---cond10
   |---cond11
   |---crazy01
   |---crazy02
   |-smoothgrid2
   |-visit
```

- `data` directory contains information about the site in various files
- `mads` directory contains various versions of the inverse analyses executed for W17 (see below)
- `smoothgrid2` directory contains information about the computational grid used by the simulator (FEHM)
- `visit` directory contains visualization files (some of the visualization are performed using VisIT; https://visit.llnl.gov; additional visualization are done using Julia graphical packages)

Versions of the W17 inverse analyses:
-------

- anew01: based on sd10.jl from waffle2014, but adds the crin/crex calibration targets
- anew02: based on sd08b.jl from waffle2014, but adds the crin/crex calibration targets
- crazy01: adds conditioning points based on 24-hour pumping tests conducted at the site wells
- crazy02: based on crazy01, but add init_min and init_max
- cond03: based on crazy02, but fixes the pilot/conditioning point craziness, and fixes an issue with optimizable parameters in the mads file
- cond04: copy of cond03 start point, but adds the calibrated source from crazy01
- cond05: picks up where cond03 left off, but makes PM-4 permeability an optimizable parameter
- cond06: based on cond03, but make PM-04 permeability an optimizable parameter
- cond07: based on cond03, but adds wiggle room around the conditioning points (factor of 10 at the supply wells, factor of 2 elsewhere), and fixes some issues with the init_min/max's
- cond08: starts where cond05 left off, but adds wiggle room (factor of 2 in kx/ky at monitoring wells, factor of 10 in kz at monitoring wells, factor of 10 in kx/ky at supply wells)
- cond05a: picks up where cond05 left off, but adds 2016 observations for crpz1, crpz4, crpz5
- cond09: copy of cond03, but adds 2016 observations for crpz1, crpz4, and crpz5
- cond10: picks up where cond09 left off, but fixes the weights for the crin/crpz Cr observations
- cond11: starts where cond09 started, but fixes the weights for the crin/crpz Cr observations (like -
- cond10, but starts where cond09 started instead of where cond09 finished)

Each version of the W17 inverse analyses is a subdirectory under the directory called `mads` (see also the directory structure above).

Commits:
--------

All the model changes are recorded using version-control system `git`.
All the commits are labeled where information about date, time and author are permanently stored.
Each commit also has a unique identifier.
For up-to-data information execute `git log` in the W17 directory.

```
* 7c43d2d - Fri, 5 May 2017 20:38:40 -0600 (12 hours ago) (HEAD -> master, origin/master, origin/HEAD)
|           readme.md added - monty
* ca46c12 - Wed, 3 May 2017 16:48:24 -0600 (3 days ago) (wc/master)
|           cond10 done; no much of progress; cond11 is still moving - monty
* f7e8473 - Wed, 19 Apr 2017 14:44:04 -0600 (2 weeks ago)
|           urgh - monty
* e54daab - Wed, 19 Apr 2017 08:17:58 -0600 (2 weeks ago)
|           add cond11 - Daniel O'Malley
* 2149cdd - Wed, 19 Apr 2017 07:00:29 -0600 (2 weeks ago)
|           add cond10 - Daniel O'Malley
* 6ffdc95 - Tue, 18 Apr 2017 13:22:39 -0600 (3 weeks ago)
|           update cond08 - Daniel O'Malley
* 8c3356e - Tue, 18 Apr 2017 08:04:19 -0600 (3 weeks ago)
|           add final cond08 sampling set up - Daniel O'Malley
* 4e5a721 - Tue, 18 Apr 2017 07:39:38 -0600 (3 weeks ago)
|           update cond08 plotting for new chain - Daniel O'Malley
* dbd76ae - Tue, 18 Apr 2017 07:14:23 -0600 (3 weeks ago)
|           add 100 step chain for cond08 - Daniel O'Malley
* 75b5312 - Mon, 17 Apr 2017 08:16:15 -0600 (3 weeks ago)
|           cond09 done - monty
* 489c132 - Sun, 16 Apr 2017 08:24:24 -0600 (3 weeks ago)
|           w01-v06 added - monty
*   0fd5230 - Tue, 11 Apr 2017 18:17:04 -0600 (4 weeks ago)
|\            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| * 83a30ff - Tue, 11 Apr 2017 18:16:57 -0600 (4 weeks ago)
| |           .gitignore modified - monty
* |   a4fc408 - Tue, 11 Apr 2017 18:14:18 -0600 (4 weeks ago)
|\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| |/
| * 493d163 - Tue, 11 Apr 2017 18:13:15 -0600 (4 weeks ago)
| |           inter results - monty
* | 758215a - Tue, 11 Apr 2017 15:09:03 -0600 (4 weeks ago)
| |           add mass estimate code - Daniel O'Malley
* | 5c3a5eb - Wed, 29 Mar 2017 08:34:31 -0600 (5 weeks ago)
| |           sorry; file modes - monty
* |   1ae4b07 - Mon, 27 Mar 2017 19:50:50 -0600 (6 weeks ago)
|\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| |/
| * 1947935 - Mon, 27 Mar 2017 19:07:36 -0600 (6 weeks ago)
| |           w01-v03 added - monty
* | 3a332b1 - Mon, 27 Mar 2017 19:36:54 -0600 (6 weeks ago)
| |           .gitconfig added - monty
* |   06d93cc - Sun, 26 Mar 2017 06:59:54 -0600 (6 weeks ago)
|\ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| * | 48f3603 - Thu, 23 Mar 2017 16:24:46 -0600 (6 weeks ago)
| | |           uq2meta plotting for cond08 - Daniel O'Malley
* | | 49fe0f8 - Sun, 26 Mar 2017 06:59:25 -0600 (6 weeks ago)
| | |           update machines for cond08 sampling - Daniel O'Malley
* | | 62564ed - Sun, 26 Mar 2017 06:59:03 -0600 (6 weeks ago)
|/ /            add longer cond08 chain - Daniel O'Malley
* | 61c722c - Thu, 23 Mar 2017 08:03:58 -0600 (6 weeks ago)
| |           add a chain for cond08 - Daniel O'Malley
* |   1e17531 - Mon, 20 Mar 2017 14:30:54 -0600 (7 weeks ago)
|\ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| * \   a540f23 - Mon, 20 Mar 2017 14:03:35 -0600 (7 weeks ago)
| |\ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - monty
| * \ \   ae07d59 - Mon, 20 Mar 2017 09:31:02 -0600 (7 weeks ago)
| |\ \ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - monty
| | |_|/
| |/| |
| * | | 646c946 - Mon, 20 Mar 2017 09:30:17 -0600 (7 weeks ago)
| | | |           cond09/w01-v02 intermediate - monty
* | | | 21b0e3e - Mon, 20 Mar 2017 14:23:54 -0600 (7 weeks ago)
| |_|/            update sample script - Daniel O'Malley
|/| |
* | | 82f5182 - Mon, 20 Mar 2017 10:13:06 -0600 (7 weeks ago)
| |/            update sampling code for cond08 - Daniel O'Malley
|/|
* | 91991cc - Mon, 20 Mar 2017 08:17:38 -0600 (7 weeks ago)
| |           add cond08 sampling code - Daniel O'Malley
* | 8ea98be - Mon, 20 Mar 2017 08:04:17 -0600 (7 weeks ago)
|/            add a 20 step chain for cond08 - Daniel O'Malley
* 90d33c7 - Sun, 19 Mar 2017 15:11:36 -0600 (7 weeks ago)
|           intermediate results added cond09 - monty
* b17ee30 - Fri, 17 Mar 2017 16:29:07 -0600 (7 weeks ago)
|           minor changes of cond09 - monty
* a9f443c - Fri, 17 Mar 2017 16:28:22 -0600 (7 weeks ago)
|           minor changes of cond09 - monty
*   b5736ea - Fri, 17 Mar 2017 16:25:25 -0600 (7 weeks ago)
|\            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| * 2bd16e4 - Fri, 17 Mar 2017 16:24:51 -0600 (7 weeks ago)
| |           cond05a/w01-v02 added - monty
* | d995d9b - Fri, 17 Mar 2017 15:57:52 -0600 (7 weeks ago)
| |           add cond09 - Daniel O'Malley
* |   8c411c6 - Fri, 17 Mar 2017 09:30:22 -0600 (7 weeks ago)
|\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| |/
| * ee8025c - Fri, 17 Mar 2017 09:30:07 -0600 (7 weeks ago)
| |           w01-v01 terminated - monty
* |   2c53929 - Thu, 16 Mar 2017 15:28:41 -0600 (7 weeks ago)
|\ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - monty
| |/
|/|
| * ac89f50 - Thu, 16 Mar 2017 11:43:54 -0600 (7 weeks ago)
| |           update makemovie - Daniel O'Malley
| *   e5ee5bf - Thu, 16 Mar 2017 08:55:03 -0600 (7 weeks ago)
| |\            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| * | 3781cba - Thu, 16 Mar 2017 08:54:32 -0600 (7 weeks ago)
| | |           start updates on makemovie - Daniel O'Malley
| | | *   abc1fc5 - Thu, 16 Mar 2017 15:28:26 -0600 (7 weeks ago) (refs/stash)
| | | |\            WIP on master: 98d9d16 cond05a results added - monty
| |_|/ /
|/| | |
| | | * 3defe46 - Thu, 16 Mar 2017 15:28:18 -0600 (7 weeks ago)
| |_|/            index on master: 98d9d16 cond05a results added - monty
|/| |
* | | 98d9d16 - Thu, 16 Mar 2017 15:27:34 -0600 (7 weeks ago)
| |/            cond05a results added - monty
|/|
* | bfaf935 - Wed, 15 Mar 2017 10:43:24 -0600 (7 weeks ago)
| |           slurm-script.sh added - monty
* | 41b3a8f - Wed, 15 Mar 2017 10:39:41 -0600 (7 weeks ago)
| |           calibrate-wtrw.jl delete restartdir added - monty
* | e1616b3 - Wed, 15 Mar 2017 10:08:06 -0600 (7 weeks ago)
| |           cond05a added with crpz-1,4,5 targets - monty
* | 548bbb7 - Wed, 22 Feb 2017 11:07:22 -0700 (2 months ago)
|/            fix in mads file for cond08 - Daniel O'Malley
* d8aee1c - Wed, 22 Feb 2017 10:35:54 -0700 (2 months ago)
|           add cond08 - Daniel O'Malley
* 196860c - Tue, 21 Feb 2017 11:48:58 -0700 (2 months ago)
|           cond05 done - Daniel O'Malley
*   57fdf50 - Thu, 16 Feb 2017 18:27:30 -0700 (3 months ago)
|\            Merge branch 'master' of gitlab.com:lanl/waffle2017 - monty
| * b4ddee6 - Mon, 13 Feb 2017 11:50:42 -0700 (3 months ago)
| |           update mads file for cond07 - Daniel O'Malley
| * 2d43fc4 - Mon, 13 Feb 2017 11:38:25 -0700 (3 months ago)
| |           add cond07 - Daniel O'Malley
| *   c1a51b9 - Mon, 13 Feb 2017 11:24:30 -0700 (3 months ago)
| |\            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| * | fb139d6 - Mon, 13 Feb 2017 11:24:13 -0700 (3 months ago)
| | |           update cond05 (still running) - Daniel O'Malley
* | | 739032d - Thu, 16 Feb 2017 18:26:46 -0700 (3 months ago)
| |/            cond06 done - monty
|/|
* | cff6330 - Fri, 10 Feb 2017 11:41:33 -0700 (3 months ago)
| |           cond06 results - monty
* | 0336e83 - Wed, 8 Feb 2017 16:33:39 -0700 (3 months ago)
|/            add cond06 - Daniel O'Malley
*   ff02744 - Wed, 8 Feb 2017 16:33:35 -0700 (3 months ago)
|\            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| *   7eb9c75 - Wed, 8 Feb 2017 09:44:28 -0700 (3 months ago)
| |\            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| | * 65e6ba1 - Wed, 8 Feb 2017 09:44:03 -0700 (3 months ago)
| | |           cond04 done not much progress - monty
* | | 5556ed6 - Wed, 8 Feb 2017 16:33:18 -0700 (3 months ago)
|/ /            add (incomplete) cond05 iterationresults - Daniel O'Malley
* |   456bca4 - Mon, 6 Feb 2017 09:06:24 -0700 (3 months ago)
|\ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| |/
| * 8bd3a15 - Sun, 5 Feb 2017 08:17:20 -0700 (3 months ago)
| |           why restart was set to false? - monty
| * a9af002 - Sun, 5 Feb 2017 08:14:40 -0700 (3 months ago)
| |           why restart was set to false? - monty
| *   43c9eff - Sat, 4 Feb 2017 12:56:43 -0700 (3 months ago)
| |\            merge - monty
| | * 432c561 - Sat, 4 Feb 2017 12:51:09 -0700 (3 months ago)
| | |           results added - monty
| * |   1840761 - Sat, 4 Feb 2017 12:54:49 -0700 (3 months ago)
| |\ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - monty
| * | | 14d459e - Sat, 4 Feb 2017 12:54:45 -0700 (3 months ago)
| | | |           fixes - monty
* | | | 27e014c - Mon, 6 Feb 2017 09:05:41 -0700 (3 months ago)
| |/ /            add code for plotting sources - Daniel O'Malley
|/| |
* | | b56dfa3 - Sun, 29 Jan 2017 07:59:58 -0700 (3 months ago)
| | |           add cond05 - Daniel O'Malley
* | |   8bd81c8 - Sun, 29 Jan 2017 07:53:05 -0700 (3 months ago)
|\ \ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| | |/
| |/|
| * | b5d057d - Fri, 20 Jan 2017 15:56:48 -0700 (4 months ago)
| | |           add cond04 - Daniel O'Malley
| * |   b495ae3 - Fri, 20 Jan 2017 15:54:24 -0700 (4 months ago)
| |\ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| | * \   6cfeec9 - Fri, 20 Jan 2017 11:11:09 -0700 (4 months ago)
| | |\ \            Merge branch 'master' of wf-fe:/net/scratch2/vvv/waffle2017 - monty
| | | * | 3ab68ca - Fri, 20 Jan 2017 11:10:04 -0700 (4 months ago)
| | | | |           results - monty
| * | | | 7154a31 - Fri, 20 Jan 2017 15:54:17 -0700 (4 months ago)
| | | | |           update readme - Daniel O'Malley
* | | | | cdc9fb0 - Sun, 29 Jan 2017 07:52:49 -0700 (3 months ago)
| |/ / /            update cond03 - Daniel O'Malley
|/| | |
* | | | 94e2d11 - Thu, 19 Jan 2017 13:45:59 -0700 (4 months ago)
| | | |           update makemovie.jl - Daniel O'Malley
* | | | faa552d - Thu, 19 Jan 2017 11:52:39 -0700 (4 months ago)
| | | |           update cond03 calibrate - Daniel O'Malley
* | | | 01107df - Thu, 19 Jan 2017 08:05:08 -0700 (4 months ago)
|/ / /            pilotpoint fix for cond03 - Daniel O'Malley
* | | fdf98c3 - Wed, 18 Jan 2017 14:22:11 -0700 (4 months ago)
| | |           remove superfluous nodes files - Daniel O'Malley
* | | 7da2fd2 - Wed, 18 Jan 2017 14:15:23 -0700 (4 months ago)
| | |           another attempt to fix R-36 - Daniel O'Malley
* | | b333d94 - Wed, 18 Jan 2017 14:00:15 -0700 (4 months ago)
| | |           fix pilot points for cond03, rename cond01/2 - Daniel O'Malley
* | | 7464e89 - Wed, 18 Jan 2017 12:45:28 -0700 (4 months ago)
| | |           add a mads file in cond01 that adds more opt params - Daniel O'Malley
* | | ee1c51e - Wed, 18 Jan 2017 12:42:38 -0700 (4 months ago)
| | |           add cond03 - Daniel O'Malley
* | | 4423e88 - Wed, 18 Jan 2017 12:37:52 -0700 (4 months ago)
| | |           fix R-36 on cond02 (hopefully) - Daniel O'Malley
* | | 43f5cb9 - Wed, 18 Jan 2017 11:15:58 -0700 (4 months ago)
| | |           fix for cond01 R-36 (hopefully) - Daniel O'Malley
* | | b8c8bbb - Tue, 17 Jan 2017 13:12:56 -0700 (4 months ago)
|/ /            update sanitycheck - Daniel O'Malley
* | 3f33503 - Thu, 12 Jan 2017 18:30:56 -0700 (4 months ago)
| |           results added - monty
* | c6ad357 - Wed, 11 Jan 2017 18:21:21 -0700 (4 months ago)
| |           calibrate-random.jl added - monty
* |   119b8e5 - Wed, 11 Jan 2017 18:16:30 -0700 (4 months ago)
|\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| * | 9729aec - Wed, 11 Jan 2017 18:15:47 -0700 (4 months ago)
| | |           w01-v02 done; w01-v03 started - monty
* | | 0e6a495 - Wed, 11 Jan 2017 14:35:07 -0700 (4 months ago)
| | |           add cond02 - Daniel O'Malley
* | | 48c7c71 - Wed, 11 Jan 2017 14:29:53 -0700 (4 months ago)
| | |           update gitignore - Daniel O'Malley
* | |   5468ff3 - Wed, 11 Jan 2017 10:55:28 -0700 (4 months ago)
|\ \ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| * \ \   8670a77 - Tue, 10 Jan 2017 11:50:45 -0700 (4 months ago)
| |\ \ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - monty
| | |/ /
| |/| |
| * | | 3e7aa2f - Tue, 10 Jan 2017 11:50:25 -0700 (4 months ago)
| | | |           w01-v02 added - monty
| * | | ddb6d65 - Tue, 10 Jan 2017 11:49:56 -0700 (4 months ago)
| | | |           w01-v02 added - monty
* | | | 5defae3 - Wed, 11 Jan 2017 10:55:17 -0700 (4 months ago)
| |/ /            new viz - Daniel O'Malley
|/| |
* | | e9e43cf - Mon, 9 Jan 2017 14:39:18 -0700 (4 months ago)
| | |           work on sanitycheck.jl - Daniel O'Malley
* | |   cf8a4f2 - Mon, 9 Jan 2017 13:49:50 -0700 (4 months ago)
|\ \ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| |/ /
| * | 4cae22a - Mon, 9 Jan 2017 08:38:25 -0700 (4 months ago)
| | |           cond01 done - monty
| * |   29d25af - Fri, 6 Jan 2017 09:05:19 -0700 (4 months ago)
| |\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| | * | 3cb0e90 - Fri, 6 Jan 2017 09:04:48 -0700 (4 months ago)
| | | |           r1-v01 new results - monty
* | | | 4b12bff - Mon, 9 Jan 2017 13:49:20 -0700 (4 months ago)
| | | |           replace symlinks with files - Daniel O'Malley
* | | | 270dd81 - Mon, 9 Jan 2017 13:42:55 -0700 (4 months ago)
|/ / /            update readme - Daniel O'Malley
* | | f369243 - Thu, 5 Jan 2017 07:46:50 -0700 (4 months ago)
| | |           add supply wells to cond01 - Daniel O'Malley
* | | 11187ef - Thu, 5 Jan 2017 07:41:23 -0700 (4 months ago)
| | |           add hyco data - Daniel O'Malley
* | | beba4ff - Wed, 4 Jan 2017 16:15:00 -0700 (4 months ago)
| | |           remove superfluous files - Daniel O'Malley
* | | 792133d - Wed, 4 Jan 2017 16:12:03 -0700 (4 months ago)
|/ /            add model with conditioning from 24 hour tests - Daniel O'Malley
* | 4d7191f - Tue, 3 Jan 2017 09:31:08 -0700 (4 months ago)
| |           2016 results - monty
* | d009400 - Wed, 21 Dec 2016 16:28:23 -0700 (5 months ago)
| |           r1-v01 results added - monty
* | 94412da - Tue, 20 Dec 2016 15:00:45 -0700 (5 months ago)
| |           anew05 results added - monty
* | 1be705f - Tue, 20 Dec 2016 14:59:42 -0700 (5 months ago)
|/            results added - monty
* 65b9863 - Thu, 15 Dec 2016 08:55:10 -0700 (5 months ago)
|           w03-v01 added - monty
* 8f4046d - Sun, 11 Dec 2016 10:05:15 -0700 (5 months ago)
|           more results added - monty
* 8f30733 - Thu, 8 Dec 2016 10:19:33 -0700 (5 months ago)
|           results added; progress made with random init guesses - monty
*   98170d0 - Sun, 4 Dec 2016 15:22:22 -0700 (5 months ago)
|\            Merge branch 'master' of gitlab.com:lanl/waffle2017 - monty
| *   19f9ead - Sat, 26 Nov 2016 09:33:56 -0700 (5 months ago)
| |\            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| * \   a0f9688 - Fri, 25 Nov 2016 08:58:48 -0700 (5 months ago)
| |\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| * \ \   8fd89f6 - Thu, 24 Nov 2016 10:27:44 -0700 (5 months ago)
| |\ \ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
* | \ \ \   d7ea745 - Sun, 4 Dec 2016 15:20:28 -0700 (5 months ago)
|\ \ \ \ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| |/ / / /
|/| | | |
| * | | | d213b8d - Sun, 4 Dec 2016 15:17:52 -0700 (5 months ago)
| | |_|/            results addded - monty
| |/| |
| * | | f559881 - Sat, 26 Nov 2016 09:33:41 -0700 (5 months ago)
| | |/            fixes - monty
| |/|
| * | c9f8443 - Fri, 25 Nov 2016 08:58:26 -0700 (5 months ago)
| |/            fixes - monty
| * b87820f - Thu, 24 Nov 2016 09:46:23 -0700 (5 months ago)
| |           runmodelcode.jl changes - monty
* |   57288e8 - Wed, 23 Nov 2016 17:20:58 -0700 (5 months ago)
|\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| |/
| * f34700b - Wed, 23 Nov 2016 17:15:33 -0700 (5 months ago)
| |           anew06 added - monty
* | 8b22567 - Wed, 23 Nov 2016 09:10:36 -0700 (5 months ago)
| |           update anew05 - Daniel O'Malley
* | 5b1e499 - Tue, 22 Nov 2016 16:49:33 -0700 (6 months ago)
| |           update anew05 - Daniel O'Malley
* | eb3a2fa - Tue, 22 Nov 2016 16:00:06 -0700 (6 months ago)
|/            add anew05 - Daniel O'Malley
*   90e9b7b - Tue, 15 Nov 2016 08:53:36 -0700 (6 months ago)
|\            Merge branch 'master' of gitlab.com:lanl/waffle2017 - Daniel O'Malley
| * 9cd6007 - Mon, 14 Nov 2016 11:32:34 -0700 (6 months ago)
| |           calibrate-random2.jl added - monty
| * a6fe3ad - Mon, 14 Nov 2016 11:30:45 -0700 (6 months ago)
| |           minor - monty
* | 08d9790 - Tue, 15 Nov 2016 08:53:20 -0700 (6 months ago)
|/            add CrIN-6 - Daniel O'Malley
* 8b37d96 - Mon, 14 Nov 2016 11:16:15 -0700 (6 months ago)
|           update grid with new cr well zones - Daniel O'Malley
* 0d8cfec - Wed, 9 Nov 2016 16:32:37 -0700 (6 months ago)
|           anew04 calibraterandom setup - monty
*   0002ada - Wed, 9 Nov 2016 14:33:46 -0700 (6 months ago)
|\            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| * 9110cb0 - Wed, 9 Nov 2016 14:32:00 -0700 (6 months ago)
| |           w01-v03 completed - monty
* |   d7baea8 - Tue, 8 Nov 2016 12:35:40 -0700 (6 months ago)
|\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| |/
| * 7f7ff40 - Tue, 8 Nov 2016 12:34:35 -0700 (6 months ago)
| |           anew03/w01-v03.mads added - monty
* | 9ca9233 - Mon, 7 Nov 2016 17:57:49 -0700 (6 months ago)
| |           minor - monty
* | a2a9b0b - Mon, 7 Nov 2016 17:50:30 -0700 (6 months ago)
| |           makemovie / fix_visit_session_path fixed - monty
* | 14bc3e9 - Mon, 7 Nov 2016 08:11:01 -0700 (6 months ago)
| |           runmodelcode.jl modified to use Mads.krige - monty
* |   da94fac - Mon, 7 Nov 2016 07:12:32 -0700 (6 months ago)
|\ \            Merge branch 'master' of wc-fe:/net/scratch2/vvv/waffle2017 - monty
| |/
| * 3f5d3b3 - Mon, 7 Nov 2016 07:10:58 -0700 (6 months ago)
| |           anew03 added - monty
* |   639322f - Sat, 5 Nov 2016 07:49:36 -0600 (6 months ago)
|\ \            Merge branch 'master' of gitlab.com:lanl/waffle2017 - monty
| |/
|/|
| * 5c07af5 - Fri, 4 Nov 2016 15:38:34 -0600 (6 months ago)
| |           add anew02 restart - Daniel O'Malley
* | 714c276 - Sat, 5 Nov 2016 07:47:38 -0600 (6 months ago)
|/            anew03 added - monty
* 0b71e64 - Fri, 4 Nov 2016 10:38:26 -0600 (6 months ago)
|           anew02/w01-v02.mads adde - monty
* 167e28f - Fri, 21 Oct 2016 08:28:42 -0600 (7 months ago)
|           update readme.txt - Daniel O'Malley
* e030300 - Fri, 21 Oct 2016 08:12:38 -0600 (7 months ago)
|           add anew02 - Daniel O'Malley
* ccd67a0 - Thu, 20 Oct 2016 20:03:57 -0600 (7 months ago)
|           add anew01 session file - Daniel O'Malley
* 5f5bee5 - Thu, 20 Oct 2016 20:03:42 -0600 (7 months ago)
|           add plot/movie code - Daniel O'Malley
* 4f02e5a - Thu, 20 Oct 2016 16:30:22 -0600 (7 months ago)
|           add results for anew01 - Daniel O'Malley
* f67a025 - Thu, 20 Oct 2016 14:05:21 -0600 (7 months ago)
|           results added - monty
* 8abbe6a - Tue, 27 Sep 2016 16:56:19 -0600 (7 months ago)
|           fixes - Daniel O'Malley
* ab3eff1 - Tue, 27 Sep 2016 16:01:07 -0600 (7 months ago)
|           fix - Daniel O'Malley
* 0b45ebf - Tue, 27 Sep 2016 15:23:13 -0600 (7 months ago)
            first stab at the new model - Daniel O'Malley
```