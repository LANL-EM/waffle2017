import Glob

function wellname2madsname(x)
	x = lowercase(x)
	x = replace(x, "#", "_")
	x = replace(x, "-", "")
end
function filename2madsname(x)
	wellname2madsname(x[1:end - 4])
end

#print out the observations for the mads file
filenames = Glob.glob("*.csv")
headers = ["O-04","CrIN-3","CrIN-2","PM-05","PM-04","PM-02","CrIN-5","CrIN-4","CrEX-1","R-28","CrEX-3","R-42","CrIN-1"]
pumpwellmadsnames = map(wellname2madsname, headers)
spinupstartdate = Dates.Date(2012, 1, 1)
calibstartdate = Dates.Date(2013, 1, 1)
observationsfile = open("observations", "w")
for filename in filenames
	data = readcsv(filename; skipstart=1)
	dates = map(x->Dates.Date(x[1:10]), data[:, 1])
	date2dataindex = Dict(zip(dates, 1:length(dates)))
	calibstartindex = date2dataindex[calibstartdate]
	drawdowns = map(Float64, data[:, 4:4 + length(headers)])
	obswellmadsname = filename2madsname(filename)
	for i = calibstartindex:size(data, 1)
		for j = 1:length(pumpwellmadsnames)
			write(observationsfile, "- P$((dates[i] - spinupstartdate).value)_$(obswellmadsname)_$(pumpwellmadsnames[j]): {log: false, max: 20, min: -20, target: $(data[i, j + 3] - data[calibstartindex, j + 3]), weight: $(data[i, end] == 0 || pumpwellmadsnames[j] == obswellmadsname ? 0 : 5)}\n")
		end
		write(observationsfile, "- P$((dates[i] - spinupstartdate).value)_$(obswellmadsname)_pm01: {log: false, max: 20, min: -20, target: 0, weight: 5}\n")
		write(observationsfile, "- P$((dates[i] - spinupstartdate).value)_$(obswellmadsname)_pm03: {log: false, max: 20, min: -20, target: 0, weight: 5}\n")
	end
end
close(observationsfile)

#create the inst files
if !isdir("inst")
	mkdir("inst")
end
obswellmadsnames = filename2madsname.(filenames)
for (i, pumpwellmadsname) in enumerate([pumpwellmadsnames; ["pm01", "pm03"]])
	instfile = open("inst/$pumpwellmadsname.inst", "w")
	instdays = calibstartdate:Dates.Date(2017, 6, 23)
	for thisday in instdays
		fehmtime = (thisday - spinupstartdate).value
		write(instfile, "@$fehmtime.000 time@ w")
		for (k, obswellmadsname) in enumerate(obswellmadsnames)
			write(instfile, " !P$(fehmtime)_$(obswellmadsname)_$(pumpwellmadsname)!")
		end
		if thisday != instdays[end]
			write(instfile, "\n")
		end
	end
	close(instfile)
end
