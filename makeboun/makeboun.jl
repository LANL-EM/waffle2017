import DataStructures
import PyCall
@PyCall.pyimport aquiferdb as db

spinupstartdate = Dates.DateTime(2012, 1, 1)
enddate = Dates.DateTime(2017, 6, 23)
allfehmtimes = Float64[]

function writeboun(wellname, outfilename, zonestring, smoothlength)
	outfile = open(outfilename, "w")
	write(outfile, "boun\n")
	write(outfile, "model 1 $wellname\n")
	db.connecttodb()
	rates, times = db.getpumpingrates(wellname, string(spinupstartdate), string(enddate))
	@show wellname, times[end]
	ratesdict = DataStructures.DefaultDict(0.0)
	ratescount = DataStructures.DefaultDict(0)
	for i = 1:length(times)
		ratesdict[times[i]] += rates[i] * 1000 / (24 * 3600)#convert from m^3/day to kg/sec
		ratescount[times[i]] += 1
	end
	for k in keys(ratesdict)
		ratesdict[k] /= ratescount[k]
	end
	rates = Array{Float64}(length(ratesdict))
	fehmtimes = Array{Float64}(length(ratesdict))
	for (i, k) in enumerate(sort(collect(keys(ratesdict))))
		rates[i] = ratesdict[k]
		fehmtimes[i] = (k - spinupstartdate).value / (1000 * 3600 * 24)
	end
	if smoothlength != false
		if unique(diff(fehmtimes)) != [1.0]
			@show smoothlength
			@show wellname
			@show unique(diff(fehmtimes))
			error("don't have data for each day, so can't smooth")
		end
		numsmoothedtimes = ceil(Int, length(ratesdict) / smoothlength)
		newrates = zeros(numsmoothedtimes)
		newfehmtimes = Array{Float64}(numsmoothedtimes)
		for i = 1:numsmoothedtimes
			newfehmtimes[i] = fehmtimes[1 + (i - 1) * smoothlength]
			newratescount = 0
			for k = 1:smoothlength
				if k + (i - 1) * smoothlength <= length(rates)
					newrates[i] += rates[k + (i - 1) * smoothlength]
					newratescount += 1
				end
			end
			newrates[i] /= newratescount
		end
		push!(newrates, 0.0)
		push!(newfehmtimes, fehmtimes[end] + 1)
		rates = newrates
		fehmtimes = newfehmtimes
	end
	@show wellname, fehmtimes[end], rates[end]
	append!(allfehmtimes, fehmtimes)
	write(outfile, "time\n")
	write(outfile, "$(length(fehmtimes))\n")
	write.(outfile, string.(fehmtimes, "\n"))
	write(outfile, "dsw\n")
	write.(outfile, string.(rates, "\n"))
	write(outfile, "end\n")
	write(outfile, "-$zonestring 0 0 1\n")
	db.disconnectfromdb()
	write(outfile, "\n")
	close(outfile)
end

writeboun("CrEX-1", "bouns/crex1.boun", "620101", false)
writeboun("CrEX-3", "bouns/crex3.boun", "620301", false)
writeboun("CrIN-1", "bouns/crin1.boun", "612101", false)
writeboun("CrIN-2", "bouns/crin2.boun", "611201", false)
writeboun("CrIN-3", "bouns/crin3.boun", "610301", false)
writeboun("CrIN-4", "bouns/crin4.boun", "618401", false)
writeboun("CrIN-5", "bouns/crin5.boun", "610501", false)
writeboun("O-04", "bouns/o4.boun", "2010004", 10)
writeboun("PM-01", "bouns/pm01.boun", "2020001", 10)
writeboun("PM-02", "bouns/pm02.boun", "2020002", 10)
writeboun("PM-03", "bouns/pm03.boun", "2020003", 10)
writeboun("PM-04", "bouns/pm04.boun", "2020004", 10)
writeboun("PM-05", "bouns/pm05.boun", "2020005", 10)
writeboun("R-28", "bouns/r28.boun", "4280001", false)
writeboun("R-42", "bouns/r42.boun", "4420001", false)

f = open("timesmacro", "w")
for t in sort(unique(allfehmtimes))
	write(f, "$t -1.2 1.0 365.25\n")
end
close(f)
