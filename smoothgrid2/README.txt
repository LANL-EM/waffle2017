V2 Stacked Waffle with smooth watertable top and embedded wells
FEHM Mesh /scratch/er/tam/er_monitoring_2016/FEHM_stack_omr_wells_v2
work directory /grids/stack_omr_wells_125m_v2/

This version eliminates neg ccoefs by using a very coarse version
of the watertable which flattens problem slopes causing neg ccoefs.
There is a small area of stair-step top along the Rio edge
where tet elements were removed that were causing neg ccoefs.
The rest of the top is smooth without stair-step effect.

This version has a flat bottom at 1000m instead of 0. 
The template triangulation for stacking layers is unchanged from v1.

number of nodes =      766283         
number of elements =  4659062    
number nodes per layer = 17416
total layers = 44 (prism converted to tets)

 NAME  MIN              MAX                DIFFERENCE
 xic   4.899750000E+05  5.101000000E+05    2.012500000E+04
 yic   5.267500000E+05  5.430000000E+05    1.625000000E+04
 zic   1.000000000E+03  1.924672718E+03    9.246727184E+02

========== FEHM Files ============================

-rw-r--r-- 1 tamiller sft 245427694 Jul 19 14:30 tet.fehmn
-rw-r--r-- 1 tamiller sft 207858176 Jul 19 14:34 tet_bin.stor
-rw-r--r-- 1 tamiller sft 279298320 Jul 19 14:36 tet.stor

========== ZONE Files ============================

Materials from WC15c GFM 
==>  tet_material.zone

WC15   Name       #nodes    #nodes/nodes  Percent
14     TPF3        10130       0.013220    1.32
13      TB4        16777       0.021894    2.19
11     TVT2         6927       0.009040    0.90
10     TPF2        85404       0.111452   11.15
 9      TPT        14388       0.018776    1.88
 8     TVT1        14546       0.018983    1.90
 7     TJFP        37163       0.048498    4.85
 6      TVK         1602       0.002091    0.21
 5      TB2        60876       0.079443    7.94
 4     TCAR       386074       0.503827   50.38
 3      TB1          573       0.000748    0.07
 2      TTC       124957       0.163069   16.31
 1     BEDR         6866       0.008960    0.90

Well zones

List of top screen well zone, distance from real to mesh
real elevation, mesh elevation, and well name 
    well_screen_tops.txt

==> well_screens.zonn <== node list for each well zone interval

==> well_screen_tops.txt <== list of wells and top screen elev stats
  zone_numr  zdist      data elev    mesh elev  name

Outside zones 
==> out_west.zonn   <== zonn 000003   west  nnum 3652
==> out_sw.zonn     <== zonn 000004   swest nnum 5456
==> out_east.zonn   <== zonn 000005   east  nnum 9891
==> out_north.zonn  <== zonn 000006   north nnum 7040

List of top node_number, x, y, z
    out_top.nodes_xyz

Note: Top of this mesh has a small area of stairstep along rio
      where tets were removed that were causing neg ccoefs

These top and bottom are full and include shared side edges
==> out_top_all.zonn    <== zonn 000001     top_all nnum 17431
==> out_bottom_all.zonn <== zonn 000002     bot_all nnum 17416

These top and bottom do not include shared side edges
==> out_top.zonn        <== zonn 000001     top    nnum 16837
==> out_bottom.zonn     <== zonn 000002     bottom nnum 16824

========== SOURCE DATA  ======================

Watertable elevations for top surface from Monty and WC15c

GFM materials from WC15c EarthVision model from Weston/Dan Strobridge

Well locations from mysql data base

========== MESH STATS   ======================

--------------------------------------------
elements with aspect ratio < .01:               242540
elements with aspect ratio b/w .01 and .02:          0
elements with aspect ratio b/w .02 and .05:          0
elements with aspect ratio b/w .05 and .1 :     238814
elements with aspect ratio b/w .1  and .2 :     953307
elements with aspect ratio b/w .2  and .5 :    2249926
elements with aspect ratio b/w .5  and 1. :     974475
min aspect ratio =  0.5113E-08  max aspect ratio =  0.8124E+00

epsilonvol:   6.7145595E-02
---------------------------------------
element volumes b/w  0.7071E-01 and  0.1308E+01:     67114
element volumes b/w  0.1308E+01 and  0.2420E+02:     38798
element volumes b/w  0.2420E+02 and  0.4477E+03:    122379
element volumes b/w  0.4477E+03 and  0.8283E+04:    764130
element volumes b/w  0.8283E+04 and  0.1532E+06:   3652581
element volumes <=  epsilonvol:     14060
element volumes <   epsilonvol:     14060
min volume =   6.6149088E-03  max volume =   1.5324024E+05
-----------------------------------------------------------

AMatbld3d_stor: *****Zero Negative Coefficients ******                          
AMatbld3d_stor: npoints =   766283  ncoefs =   11127731                         
AMatbld3d_stor: Number of unique coefs =    661645                              
AMatbld3d_stor: Maximum num. connections to a node =    21                 
AMatbld3d_stor: Volume min =   2.4845111E+03                                    
AMatbld3d_stor: Volume max =   7.0918830E+05                                    
AMatbld3d_stor: Total Volume:   1.7185007E+11                                   
AMatbld3d_stor: abs(Aij/xij) min =   0.0000000E+00                              
AMatbld3d_stor: abs(Aij/xij) max =   2.6040942E+03                              
AMatbld3d_stor: (Aij/xij) max =   0.0000000E+00                                 
AMatbld3d_stor: (Aij/xij) min =  -2.6040942E+03                                 
AMatbld3d_stor Matrix coefficient values stored as scalar area/distance         
AMatbld3d_stor Matrix compression used for graph and coefficient values         
ascii STOR file written with name tet.stor                              
*** SPARSE COEFFICIENT MATRIX _astor SUCCESSFUL ***                             


creens in well_screens.zonn =======================
Compare 2014 and 2016 well_screens.zonn

Most are similar given the resolution differences.

The 2014 pumping wells are full mesh columns, 
The 2016 pumping wells are screen interval only ~400m.

names	  2016 screens 	2014 screens 
O-1 #1	  2010001 24	2010001 17
O-4 #1	  2010004 24	2010004 20
PM-1 #1	  2020001 28	2020001 22
PM-2 #1	  2020002 26	2020002 27
PM-3 #1	  2020003 29	2020003 25
PM-4 #1	  2020004 28	2020004 25
PM-5 #1	  2020005 28	2020005 26
PM-6 #1	  2020006 0	2020006 25
R-1 #1	  4010001 2	4010001 2
R-8 #1	  4080001 4	4080001 0
R-8 #2	  4080002 1	4080002 0
R-11 #1	  4110001 2	4110001 2
R-13 #1	  4130001 4	4130001 4
R-15 #1	  4150001 4	4150001 4
R-28 #1	  4280001 2	4280001 2
R-33 #1	  4330001 2	4330001 2
R-33 #2	  4330002 1	4330002 1
R-34 #1	  4340001 2	4340001 2
R-35a #1  4350101 2	4350101 3
R-35b #1  4350201 2	4350201 2
R-36 #1	  4360001 2	4360001 2
R-42 #1	  4420001 2	4420001 2
R-43 #1	  4430001 2	4430001 2
R-43 #2	  4430002 1	4430002 1
R-44 #1	  4440001 1	4440001 1
R-44 #2	  4440002 1	4440002 1
R-45 #1	  4450001 1	4450001 1
R-45 #2	  4450002 2	4450002 2
R-50 #1	  4500001 1	4500001 1
R-50 #2	  4500002 2	4500002 2
R-61 #1	  4610001 1	4610001 1
R-61 #2	  4610002 2	4610002 2
R-62 #1	  4620001 2	4620001 2

Zone numbers differ:
SCI-1 #1   7010001 2	7010301 1
SCI-2 #1   7020001 2	7010302 1
MCOI-4 #1  7040001 2	7020304 1
MCOI-5 #1  7050001 1	7020305 1
MCOI-6 #1  7060001 2	7020306 1

New wells:
SIMR-2 #1  7050201 2	
CrEX-1 #1   620101 4	8080000 6
CrEX-2 #1   620201 3	8190000 6
CrEX-3 #1   620301 3	8210000 6
CrPZ-1 #1   630101 1	
CrPZ-2 #1   630201 1	
CrPZ-2 #2   630202 2	
CrPZ-3 #1   630301 2	
CrPZ-4 #1   630401 2	
CrPZ-5 #1   630501 2	
CrIN-1 #1   612101 3	
CrIN-2 #1   611201 3	
CrIN-3 #1   610301 3	
CrIN-4 #1   618401 3	
CrIN-5 #1   610501 3	
CrIN-6 #1   610601 3	

