ARGS=Array{String,1}(0)
push!(ARGS, "cond11/w01-v10.mads")
push!(ARGS, "../../../smoothgrid2/tet.geo")
info("Mads file: $(ARGS[1])")
info("Grid file: $(ARGS[2])")

include("remediationscenarios.jl")