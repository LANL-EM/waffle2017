import CrPlots
import FEHM
import Interpolations

basedir = "/lclscratch/monty/waffle2017/mads/cond11_pwl1"
mydir = "." # configure 'mydir' to a directory where .jld and pumpingfigs can be saved!

# Create directory to store figures
if !isdir(mydir)
	mkdir(joinpath(mydir,"pumpingfigs"))
end

wells = ["pm1", "pm2", "pm3", "pm4", "pm5", "o4"]

for well in wells
	jldfilename = "$mydir/w01_$well.jld"
	
	# Create JLD if not present
	if !isfile(jldfilename)
		FEHM.avs2jld("../smoothgrid2/tet.geo", "$basedir/$well/w01", jldfilename)#; suffix="_sca_node.avs")
	end
	
	spinupendday = 365
	wl, times, xs, ys, zs = JLD.load(jldfilename, "WL", "times", "xs", "ys", "zs")
	wlinterp = Interpolations.interpolate((times,), wl, Interpolations.Gridded(Interpolations.Linear()))
	dd(t) = wlinterp[spinupendday] - wlinterp[t]
	topnodesfilename = joinpath("..", "smoothgrid2", "out_top.nodes_xyz")
	topnodes = map(Int, readdlm(topnodesfilename)[:, 1])

	x0 = CrPlots.getwelllocation("PM-05")[1] - 250
	x1 = CrPlots.getwelllocation("PM-01")[1] + 250
	y0 = CrPlots.getwelllocation("PM-02")[2] - 100
	y1 = CrPlots.getwelllocation("O-04")[2] + 100
	boundingbox = (x0, y0, x1, y1)
	plotwells = [["CrPZ-$i" for i in 1:5]; ["CrIN-$i" for i in 1:6]; ["CrEX-$i" for i in 1:4]; ["R-61", "R-42", "R-28", "R-50", "R-44", "R-45", "R-13", "SIMR-2", "R-43", "R-62", "R-15", "PM-01", "PM-02", "PM-03", "PM-04", "PM-05", "O-04", "R-11", "R-36"]]

	date0 = Dates.Date(2010, 5, 30)

	# Configure placement of overlay objects
	lmin_x = 496278.281759 # minX of canvas
	lmax_x = 501949.141159 # maxX of canvas
	delta_x = 500.0 # Offset from left/right of canvas
	w = 1000.0 # Element width
	ly0 = boundingbox[2] + 250 # y component
	c_p = [lmin_x+delta_x,ly0] # colorbar position
	m_p = [lmax_x-delta_x-w,ly0] # meter position
	dp = [0.32,0.06,0.7*0.2,0.0135] # pbar position, width, height

	upperlimit = 0.5
	lowerlimit = -0.5
	plottimes = collect(spinupendday:90:maximum(times))
	for i = plottimes
		@show well, i
		plotdata = map(x->max(x, -0.5), dd(i)[topnodes])
		@show extrema(plotdata)
		fig, ax, img = CrPlots.crplot(boundingbox, xs[topnodes], ys[topnodes], plotdata; upperlimit=upperlimit, lowerlimit=lowerlimit, cmap=CrPlots.rainbow, alpha=0.5)
		CrPlots.addwells(ax, plotwells; alpha=0.8, xoffset=22, yoffset=22, smartoffset=true, fontsize=12)
		CrPlots.addcbar_horizontal(ax, CrPlots.rainbow, "	Δh", c_p[1], c_p[2], CrPlots.getticks([lowerlimit, upperlimit]), lowerlimit, upperlimit)
		CrPlots.addmeter(ax, m_p[1], m_p[2], [250, 500, 1000], ["250 m", "500 m", "1 km"], textoffsety=90)
		plotdate = date0 + Dates.Day(i)
		CrPlots.addpbar(fig, ax, (i - spinupendday) / (maximum(plottimes) - spinupendday), "		  $plotdate",pbar_x0=dp[1],pbar_y0=dp[2],pbar_height=dp[4],pbar_width=dp[3],fontsize=12)
		#display(fig); println()
		filename = "$mydir/pumpingfigs/drawdown_$(well)_$(lpad(Int(i), 4, 0)).png"
		println("filename = $filename")
		fig[:savefig](filename, dpi=120)
		PyPlot.close(fig)
	end
end
