import YAML

include("gettrends.jl")

f = open("concobs.yaml")
concobs = YAML.load(f)
close(f)

slopes = gettrends(concobs)
#- conc_r11_2005: {log: false, max: 10000.0, min: 0.0, target: 20.33, weight: 1.1150610177494726}
for k in keys(slopes)
	println("- $k: {log: false, target: $(slopes[k]), weight: 10}")
end
