import JLD
#machinenames = ["es$(lpad(i, 2, 0))" for i in 7:27 if i != 24 && i != 8 && i != 22]
#machinenames = ["es$(lpad(i, 2, 0))" for i in 7:27 if !(i in [8, 17, 19, 20, 21, 22, 23, 24])]
machinenames = ["es$(lpad(i, 2, 0))" for i in 7:27 if !(i in [24])]
const numprocspermachine = 16
if nprocs() == 1
	for i = 1:numprocspermachine
		@show i
		addprocs(machinenames)#do it this way so that the worker id's are not blocked onto each machine to help load balance
		println("adding procs")
	end
end
@show nprocs()
@everywhere workingdir = remotecall_fetch(1, ()->pwd())
@everywhere cd(workingdir)
@everywhere println(pwd())
@show nprocs()
import Mads
@show nprocs()
if !isdefined(:md)
	md = Mads.loadmadsfile("w01-v03.mads")
end
@show nprocs()
chain = Mads.emceesampling(md; numwalkers=2 * nworkers(), burnin=10 * nworkers(), nsteps=100 * nworkers(), seed=1234, weightfactor=1e-4, sigma=1e-4)
JLD.save("chain.jld", "chain", chain)
