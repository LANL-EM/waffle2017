import CrPlots 
import JLD

function setwells(filename::AbstractString="")
    wellnames = [["CrPZ-$i" for i in 1:5]; ["CrIN-$i" for i in 1:6]; ["CrEX-$i" for i in 1:3]; ["R-61", "R-42", "R-28", "R-50", "R-44", "R-45", "R-13", "SIMR-2", "R-43", "R-62", "R-15", "PM-03", "O-04", "R-11", "R-36"]]
    wells = Dict()
    for (i,wellname) in collect(enumerate(wellnames))
        wells[wellname] = Dict()
        wells[wellname]["name"] = wellname
        x,y = CrPlots.getwelllocation(wellname)
        wells[wellname]["well_x"] = x
        wells[wellname]["well_y"] = y
        wells[wellname]["xoffset"] = 10.0
        wells[wellname]["yoffset"] = 10.0
    end 
    # Specfic offset modifcations
    wellname = "CrIN-3" 
    wells[wellname]["xoffset"] = 10.0
    wells[wellname]["yoffset"] = -50.0

    wellname = "CrIN-4" 
    wells[wellname]["xoffset"] = 10.0
    wells[wellname]["yoffset"] = -40.0

    wellname = "CrPZ-1" 
    wells[wellname]["xoffset"] = -150.0
    wells[wellname]["yoffset"] = -50.0

    wellname = "CrPZ-2" 
    wells[wellname]["xoffset"] = -150.0
    wells[wellname]["yoffset"] = 20.0

    wellname = "CrPZ-3" 
    wells[wellname]["xoffset"] = 10.0
    wells[wellname]["yoffset"] = -40.0

    wellname = "CrEX-3" 
    wells[wellname]["xoffset"] = 10.0
    wells[wellname]["yoffset"] = -50.0

    wellname = "R-11"
    wells[wellname]["xoffset"] = 10.0
    wells[wellname]["yoffset"] = -50.0


	if filename != ""
		try
			JLD.save(filename, wells)
		catch
			warn("JLD file cannot be saved")
		end
	end
    return wells
end

