ARGS=Array{String,1}(0)
push!(ARGS, "cond05/w01-v05.mads")
push!(ARGS, "../../../smoothgrid2/tet.geo")
info("Mads file: $(ARGS[1])")
info("Grid file: $(ARGS[2])")

include("remediationscenarios.jl")