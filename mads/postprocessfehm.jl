import FEHM

function postprocessfehm(scenarios::Associative; rootname::AbstractString="w01", workingdir::AbstractString=".", force::Bool=false, timefilter::Function=t->true, geofile::AbstractString=joinpath(workingdir, "..", "..", "smoothgrid2", "tet.geo"))
	for scenarioname in keys(scenarios)
		if !isfile("$workingdir/$(rootname)_$(scenarioname).jld") || force==true
			info("Creating $workingdir/$(rootname)_$(scenarioname).jld ...")
			FEHM.avs2jld(geofile, "$workingdir/wl_$(scenarioname)/$rootname", "$workingdir/$(rootname)_$(scenarioname).jld"; timefilter=timefilter)
		else
			warn("File $workingdir/$(rootname)_$(scenarioname).jld already exists! Post processing skips ...")
		end
	end
end