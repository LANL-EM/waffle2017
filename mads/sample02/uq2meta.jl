import Mads
using LaTeXStrings
import PyCall
@PyCall.pyimport aquiferdb as db
madsfilename = "w01-v01.mads"
chainfilename = "chain.jld"
if !isdefined(:md)
	md = Mads.loadmadsfile(madsfilename)
end
wellnames = ["R-11", "R-13", "R-15", "R-28", "R-33#1", "R-33#2", "R-35b", "R-36", "R-42", "R-43#1", "R-43#2", "R-44#1", "R-44#2", "R-45#1", "R-45#2", "R-50#1", "R-50#2", "R-61#1", "R-61#2", "R-62", "CrEX-1", "CrEX-3", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6", "CrPZ-1", "CrPZ-2#1", "CrPZ-2#2", "CrPZ-3", "CrPZ-4", "CrPZ-5"]
thinning = 10
chain, llhoods = JLD.load(chainfilename, "chain")
chain = chain[:, 1:thinning:end]
llhoods = llhoods[1:thinning:end]

if !isdefined(:results)
	results = Mads.forward(md, chain; checkpointfrequency=416)
end

if !isdefined(:organizedata)
	organizedata = true
end
obs2time(obsname) = DateTime(parse(Int, split(obsname, "_")[end]), 7)
function obs2well(obsname)
	wellpart = join(split(obsname, "_")[2:end-1], "_")
	if wellpart[1] == 'r'
		result = "R-" * wellpart[2:end]
	elseif wellpart[1] == 'c'
		result = "Cr" * uppercase(wellpart[3:4]) * "-" * wellpart[5:end]
	else
		error("weird well name: $wellpart")
	end
	return replace(result, "_", "#")
end
if organizedata
	conc_index = Mads.indexkeys(md["Observations"], "conc")
	conc_keys = Mads.filterkeys(md["Observations"], "conc")
	wellobs = Dict{Any, Array{Float64, 2}}()
	welltimes = Dict{Any, Array{DateTime, 1}}()
	for (i, obsname) in enumerate(conc_keys)
		if !endswith(obsname, "slope")#make sure it isnt one of the conc_*_slope things
			time = obs2time(obsname)
			well = obs2well(obsname)
			if !haskey(wellobs, well)
				wellobs[well] = results[:, conc_index[i]]''
				welltimes[well] = Float64[time]
			else
				wellobs[well] = hcat(wellobs[well], results[:, conc_index[i]]'')
				push!(welltimes[well], time)
			end
		end
	end
	organizedata = false
end

if !isdefined(:dodatabase)
	dodatabase = true
end
if dodatabase
	dbobs = Dict()
	dbobstimes = Dict()
	dmean = Dict()
	wellname(fullname) = split(fullname, "#")[1]
	portdesc(fullname) = length(split(fullname, "#")) == 1 ? "SINGLE COMPLETION" : split(fullname, "#")[2]
	db.connecttodb()
	for i = 1:length(wellnames)
		dbobs[wellnames[i]], dbobstimes[wellnames[i]] = db.getchromiumconcentrations(wellname(wellnames[i]), portdesc(wellnames[i]))
		if length(dbobstimes[wellnames[i]]) == 0
			dbobs[wellnames[i]], dbobstimes[wellnames[i]] = db.getchromiumconcentrations(wellname(wellnames[i]), portdesc(wellnames[i]), fullchemdb=true, filterresults=false)
		end
		ind = find(dbobstimes[wellnames[i]].>Date(2013, 7))
		if length(ind) > 0
			dmean[wellnames[i]] = mean(dbobs[wellnames[i]][ind])
		end
	end
	db.disconnectfromdb()
	dodatabase = false
end

plotrows = 5
plotcols = 7
fig, axs = PyPlot.subplots(plotrows, plotcols, sharey=false, sharex=true, figsize=(16, 9), dpi=240)
for (i, well) in enumerate(sort(collect(keys(wellobs))))
	@show i, length(collect(keys(wellobs)))
	ax = axs[i]
	@show length(welltimes[well])
	@time begin
		ax[:plot](welltimes[well], wellobs[well]', "k", alpha=0.02)
		if i < 6
			ax[:set_ylabel]("Cr [ppb]")
		end
		if haskey(dbobs, well)
			ax[:plot](dbobstimes[well], dbobs[well], "r.")
			if length(dbobs[well]) > 0 && max(maximum(dbobs[well]), maximum(wellobs[well])) < 10
				ax[:set_ylim](0, 10)
			end
		else
			if maximum(wellobs[well]) < 10
				ax[:set_ylim](0, 10)
			end
		end
		ax[:set_title](well)
	end
end
fig[:autofmt_xdate]()
fig[:tight_layout]()
fig[:savefig]("uq.png")
PyPlot.close(fig)
