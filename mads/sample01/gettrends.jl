function fit(cs, ts, degree)
	A = [ (ts[i] - 2005) ^ p for i = 1:length(cs), p=0:degree ]
	return A \ cs
end

function gettrends(concobs)
	wellobs = Dict()
	welltimes = Dict()
	slopes = Dict()
	for k in keys(concobs)
		if startswith(k, "conc_")
			wellid = k[6:end - 5]
			time = parse(Float64, k[end - 3:end])
			if haskey(wellobs, wellid)
				push!(wellobs[wellid], concobs[k])
				push!(welltimes[wellid], time)
			else
				wellobs[wellid] = Float64[concobs[k]]
				welltimes[wellid] = Float64[time]
			end
		end
	end
	for k in keys(wellobs)
		fitvars = fit(wellobs[k], welltimes[k], 1)
		slopes[string("conc_", k, "_slope")] = fitvars[2]
		#=
		mint = minimum(welltimes[k])
		maxt = maximum(welltimes[k])
		ts = linspace(mint, maxt, 50)
		fitcurve = map(t->fitvars[1] + fitvars[2] * t, map(t->t - 2005, ts))
		PyPlot.plot(welltimes[k], wellobs[k], "k.", ms=10)
		PyPlot.plot(ts, fitcurve)
		PyPlot.title(k)
		PyPlot.savefig("/home/omalled/figs/trends/$k.pdf")
		PyPlot.clf()
		=#
	end
	return slopes
end
