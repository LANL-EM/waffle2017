import JLD

function setscenarios(case::Number=20170916; filename::AbstractString="")
	colors = ["r", "b", "g", "c", "m", "y", "r--", "b--", "g--", "c--", "m--", "y--"]
	scenarios = Dict()
	scenarioname = "no_action"
	scenarios[scenarioname] = Dict()
	scenarios[scenarioname]["legend"] = "No Action"
	scenarios[scenarioname]["wellsinuse"] = []
	scenarios[scenarioname]["pntstartyear"] = 2017
	scenarios[scenarioname]["gpmrates"] = [[]]
	scenarios[scenarioname]["pumptimes"] = [[]]
	scenarios[scenarioname]["color"] = "k"

	if case == 20170916
		colorindex = 1
		scenarioname = "scenario_1_20170916"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 1: CrIN 1,3,5 injection"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrIN-1", "CrIN-3", "CrIN-5"]
		scenarios[scenarioname]["pntstartyear"] = 2017
		scenarios[scenarioname]["gpmrates"] = [[70, 0], [70, 0], [70, 0], [-70, 0], [-70, 0], [-70, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1
		scenarioname = "scenario_2_20170916"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 2: CrIN 2,4,6 injection"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrIN-2", "CrIN-4", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2017
		scenarios[scenarioname]["gpmrates"] = [[70, 0], [70, 0], [70, 0], [-70, 0], [-70, 0], [-70, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25], [2017, 2017 + 10 / 365.25]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1
		scenarioname = "scenario_3_20170916"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "CrIN-6 extraction"
		scenarios[scenarioname]["wellsinuse"] = ["CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2017
		scenarios[scenarioname]["gpmrates"] = [[70, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2017, 2017 + 10 / 365.25]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1
		scenarioname = "scenario_4_20170916"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "CrIN-6 injection"
		scenarios[scenarioname]["wellsinuse"] = ["CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2017
		scenarios[scenarioname]["gpmrates"] = [[-70, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2017, 2017 + 10 / 365.25]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1
	elseif case == 20171128
		colorindex = 1
		scenarioname = "scenario_7a_20171128"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 7a"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[70, 0], [70, 0], [60, 0], [80, 0], [-45, 0], [-40, 0], [-30, 0], [-50, 0], [-50, 0], [-65, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1
		scenarioname = "scenario_7b_20171128"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 7a"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[70, 0], [70, 0], [60, 0], [80, 0], [-50, 0], [-40, 0], [-40, 0], [-50, 0], [-50, 0], [-50, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1
	elseif case == 20180208
		colorindex = 1
		scenarioname = "scenario_8a_20180208"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 8a (-60 gpm)"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[75, 0], [75, 0], [60, 0], [70, 0], [-40, 0], [-40, 0], [-30, 0], [-50, 0], [-60, 0], [-60, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1

		scenarioname = "scenario_8b_20180208"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 8b (+60 gpm)"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[75, 0], [75, 0], [60, 0], [70, 0], [-70, 0], [-70, 0], [-50, 0], [-70, 0], [-80, 0], [60, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1

		scenarioname = "scenario_8c_20180208"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 8c (+80 gpm)"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[75, 0], [75, 0], [60, 0], [70, 0], [-70, 0], [-70, 0], [-50, 0], [-70, 0], [-100, 0], [80, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1

		scenarioname = "scenario_8d_20180208"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 8d (+120 gpm)"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[75, 0], [75, 0], [60, 0], [70, 0], [-75, 0], [-75, 0], [-60, 0], [-70, 0], [-120, 0], [120, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1

		scenarioname = "scenario_8e_20180208"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 8e (0 gpm)"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[75, 0], [75, 0], [60, 0], [70, 0], [-60, 0], [-60, 0], [-30, 0], [-60, 0], [-70, 0], [0, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1
		
		scenarioname = "scenario_8f_20180208"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 8f (0, 0 gpm)"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[75, 0], [75, 0], [60, 0], [70, 0], [0, 0], [-70, 0], [-70, 0], [-70, 0], [-70, 0], [0, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023], [2018, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1

		scenarioname = "scenario_8g_20180208"
		scenarios[scenarioname] = Dict()
		scenarios[scenarioname]["legend"] = "Test 8g (+60, -60 gpm)"
		scenarios[scenarioname]["wellsinuse"] = ["CrEX-1", "CrEX-2", "CrEX-3", "CrEX-4", "CrIN-1", "CrIN-2", "CrIN-3", "CrIN-4", "CrIN-5", "CrIN-6"]
		scenarios[scenarioname]["pntstartyear"] = 2018
		scenarios[scenarioname]["gpmrates"] = [[75, 75, 0], [75, 75, 0], [60, 60, 0], [70, 70, 0], [-70, -40, 0], [-70, -30, 0], [-50, -50, 0], [-70, -60, 0], [-80, -60, 0], [60, -60, 0]]
		scenarios[scenarioname]["pumptimes"] = [[2018, 2020, 2023], [2018, 2020, 2023], [2018, 2020, 2023], [2018, 2020, 2023], [2018, 2020, 2023], [2018, 2020, 2023], [2018, 2020, 2023], [2018, 2020, 2023], [2018, 2020, 2023], [2018, 2020, 2023]]
		scenarios[scenarioname]["color"] = colors[colorindex]; colorindex += 1
	end
	if filename != ""
		try
			JLD.save(filename, scenarios)
		catch
			warn("JLD file cannot be saved")
		end
	end
	return scenarios
end
