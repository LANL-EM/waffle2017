import Mads

if !isdefined(:md)
	md = Mads.loadmadsfile("w01-v01.mads")
end

for k in keys(md["Observations"])
	if startswith(k, "P")
		if contains(k, "r35a") || contains(k, "r33_2")
			md["Observations"][k]["weight"] = 0.0
		end
	end
end

obs = md["Observations"]
f = open("newobs.yaml", "w")
for k in keys(md["Observations"])
	write(f, "- $k: {\"target\":$(obs[k]["target"]),\"weight\":$(obs[k]["weight"])}\n")
end
close(f)
