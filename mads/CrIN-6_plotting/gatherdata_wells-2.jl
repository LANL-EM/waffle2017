import Mads
import CrPlots
import Glob
import JLD2
import FileIO

# Loads well names from a file
@everywhere function get_well_names(filename::AbstractString)
    lines = readlines(filename)
    wells = String[] 
    for l in lines
        l = split(l)
        wellname = "$(l[1])-$(l[2])"
        println(wellname)
        push!(wells, wellname)
    end
    return wells
end

# Gets nodes from mesh whose x value is larger than x value of CrIN-6 well
@everywhere function get_bad_cr_nodes()
    println("Loading Mesh")
    xs = FileIO.load("waffle2017_data/mesh.jld2", "xs")
    println("Complete\n")
    well = "CrIN-6"
    xwell,ywell = CrPlots.getwelllocation(well)
    # Get node index for all nodes with x > xboundary
    nodes = Int64[]
    for (i,x) in enumerate(xs)
        #if x >= xwell
        if x >= 499950.0
            push!(nodes,i)
        end
    end
    return nodes
end

#get control cell volumes
@everywhere function getvolumes(filename::AbstractString)
    println("Loading Control Cell Volumes")
    f = open(filename)
    readline(f)
    readline(f)
    goodline = readline(f)
    numwrittencoeffs, numequations, ncoef_p_neq_p1, numareacoeffs = map(x->parse(Int, x), split(goodline)[1:4])
    volumes = zeros(Float64,numequations)
    node_cnt = 1
    while node_cnt <= numequations
        line = readline(f)
        line = split(line)
        for l in line
            volumes[node_cnt] = parse(Float64,l)
            node_cnt += 1
        end
    end
    close(f)
    println("Complete") 
    return volumes   
end

@everywhere function process_scenarios(scenario::AbstractString)

    info("Processing scenario "*scenario)
    chain_values = FileIO.load("conditioned_chain.jld2", "names") 
    println(chain_values)

    names = Array{String}(length(chain_values)) 
    for (i,val) in enumerate(chain_values)
        names[i] = "waffle2017_data/cond11_chain_$(lpad(val,5,0))_$(scenario).jld2"
    end 
    println(names)
    FileIO.save("$(scenario)_names.jld2", "names", names) 

    times = FileIO.load(names[1], "times")
    start_index = find(x -> x == 365.25, times)
    if length(start_index) > 1
        start_index = start_index[2]
    else
        start_index = start_index[1]
    end
    times = times[start_index:end]/365.25 + 1964

    # load node information
    bad_cr_nodes = get_bad_cr_nodes()
    points = [746435, 743696, 746514]
    info("Loading Wells")
    wells = FileIO.load("wells.jld", "wells")
    CrNodes = wells["CrIN-6-#1"]["well_nodes"][1]
    well_names = get_well_names("wells.txt") 
    info("Loading Wells Complete")

    storfile = "/lclscratch/jhyman/waffle2017/smoothgrid2/tet.stor"
    println(storfile)
    volumes = getvolumes(storfile)

    CrArray = zeros(Float64,length(names),length(times), 4 + length(well_names))
    for (n,name) in enumerate(names)
        println()
        info("Working on file: $(name)")
        index = split(name,"_")[4]
        
        info("Loading Mads file")
        md = Mads.loadmadsfile("/lclscratch/jhyman/waffle2017/mads/cond11/madsfiles/cond11_chain_$(lpad(index,5,0)).mads")
        phi =  md["Parameters"]["adv_por"]["init"] 
        info("Porosity: $(phi)\n")
        info("Loading Data") 
        Cr,times = FileIO.load(name, "Cr", "times")
        info("Complete\n")
 
        start_index = find(x -> x == 365.25, times)
        if length(start_index) > 1
            start_index = start_index[2]
        else
            start_index = start_index[1]
        end
        Cr = Cr[start_index:end]
        times = times[start_index:end]/365.25 + 1964
    
            
        for j in 1:length(times)
            # index 1 in CrArray is BadCr 
            for i in bad_cr_nodes 
                if Cr[j][i] > 50.0
                    CrArray[n,j,1] += Cr[j][i]*volumes[i]*phi*10.0^-6
                end
            end

            # index 2:4 are new points of interest  
            # change to ppb 
            for (ip,p) in enumerate(points)
                CrArray[n,j,1+ip] = Cr[j][p]
            end # End new points loop
            for (iw,well) in enumerate(well_names)
                #println(wells[well])
                if length(wells[well]["well_nodes"]) > 0
                    nodes = wells[well]["well_nodes"][1]
                    for i in nodes 
                        # change to ppb 
                        CrArray[n,j,4+iw] = Cr[j][i]
                    end
                end # End node loop
            end # End well loop

        end # End time loop
    end # End name loop
    info("Saving $(scenario)_Cr.jld2")
    FileIO.save("$(scenario)_Cr_cond.jld2", "Cr", CrArray, "times", times)
    info("Done with $(scenario)")
end

scenarios = ["w01_no_action","w01_scenario_8a_20180208", "w01_scenario_8b_20180208", "w01_scenario_8c_20180208", "w01_scenario_8d_20180208", "w01_scenario_8e_20180208"]

pmap(process_scenarios, scenarios)

