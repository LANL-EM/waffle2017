import PyPlot
using JLD2
using FileIO
using StatsBase 

## Load Chain weights
function chain_counts()
    full_chain = load("sub_chain_full.jld", "index") 
    cnts = load("chain_cnts.jld", "cnt") 
    weights = Dict()
    for (i,val) in enumerate(full_chain)
        weights[val] = cnts[i] 
    end
    return weights
end

# Loads well names from a file
function get_well_names(filename::AbstractString)
    lines = readlines(filename)
    wells = String[] 
    for l in lines
        l = split(l)
        wellname = "$(l[1])-$(l[2])"
        println(wellname)
        push!(wells, wellname)
    end
    return wells
end

titles = ["BadCr", "Point-1", "Point-2", "Point-3"]
well_file = "wells.txt"
well_names = get_well_names(well_file)
for well in well_names
    push!(titles, well)
end
weights = chain_counts()

all_lines = false

scenarios = []
push!(scenarios, Dict("scenario"=>"w01_no_action", "color"=>"r", "label"=>"Base Case"))
push!(scenarios, Dict("scenario"=>"w01_scenario_8a_20180208", "color"=>"g", "label"=>"Scenario: 8a"))
push!(scenarios, Dict("scenario"=>"w01_scenario_8b_20180208", "color"=>"k", "label"=>"Scenario: 8b"))
push!(scenarios, Dict("scenario"=>"w01_scenario_8c_20180208", "color"=>"m", "label"=>"Scenario: 8c"))
push!(scenarios, Dict("scenario"=>"w01_scenario_8d_20180208", "color"=>"b", "label"=>"Scenario: 8d"))
push!(scenarios, Dict("scenario"=>"w01_scenario_8e_20180208", "color"=>"c", "label"=>"Scenario: 8e"))

for cr_index in collect(1:length(titles))
#for cr_index in collect(1:2)
    println(titles[cr_index]) 
    fig,ax = PyPlot.subplots( figsize = (10,5))
    for scenario in scenarios
        job = scenario["scenario"]
        println(job)
    
        filename = scenario["scenario"]*"_Cr_cond.jld2"
        println("Loading $(filename)")
        Cr,times = load(filename, "Cr", "times")
        println("Loading $(filename) complete")
        ## Check for CrIN-6 > 200 at 2017
        tmp = abs.(times - 2017.0)
        val, idx = findmin(tmp)
        filename = scenario["scenario"]*"_names.jld2"
        names = load(filename, "names")
        thin_weights = zeros(Float64, length(names))
        for (i,name) in enumerate(names)
            index = parse(Int64,split(name,"_")[4])
            thin_weights[i] = weights[index]
        end

        # check for ppb > 200 at CrIN-6 at 2017
        keep = []
        n,m = size(Cr)
        for i in 1:n 
            if Cr[i,idx,end] > 200 && Cr[i,idx,end] < 350 
                push!(keep, i)    
            end
        end
        Cr = Cr[keep,:,cr_index]
        thin_weights = thin_weights[keep]

        n,m = size(Cr)
        mu = zeros(Float64,m)
        low = zeros(Float64,m)
        high = zeros(Float64,m)
 
        w = Weights(thin_weights)
        for j in collect(1:m)
            x = Cr[:,j]
            mu[j] = median(x, w)
            low[j], high[j] = quantile(Cr[:,j], w, [0.025, 0.975])
        end
 
        ax[:plot](times, mu, scenario["color"], linewidth = 2.5, label = scenario["label"])
        PyPlot.plot(times, low, scenario["color"]*"--") 
        PyPlot.plot(times, high, scenario["color"]*"--") 
        PyPlot.fill_between(times, low, high, color=scenario["color"], alpha=0.1)
 
        max_cr = 0
        max_index = 0 
        if all_lines 
            for i in collect(1:n)
                ax[:plot](times, Cr[i,:], scenario["color"], alpha = 10^2*w[i])
                if max_cr < maximum(Cr[i,:])
                    max_cr = maximum(Cr[i,:])
                    max_index = i
                end
            end
            println("max index: $(max_index)")
            println("Chain name $(names[max_index])")
            ax[:plot](times, Cr[max_index,:], "r", alpha = 1.0)
        end
    end
    
    ymin,ymax = PyPlot.ylim()
    if ymax < 10
        PyPlot.ylim(ymin, 10)
    end
    ymin,ymax = PyPlot.ylim()
    if ymin < 0
        PyPlot.ylim(0, ymax)
    end 

    PyPlot.xlabel("Year") 
    if cr_index == 1
        PyPlot.ylabel("Mass [kg]")
    else
        PyPlot.ylabel("Cr [ppb]")
    end
    PyPlot.legend(loc = "upper left")
    PyPlot.title(titles[cr_index])
    PyPlot.xlim( (1964, 2028))
    name = "figures_cond/all_times/"*titles[cr_index]*"_all_scenarios.png"
    println("Saving $(name)")
    PyPlot.savefig(name)
    PyPlot.xlim( (2010, 2028))
    name = "figures_cond/zoom/"*titles[cr_index]*"_all_scenarios_zoom.png"
    println("Saving $(name)")
    PyPlot.savefig(name)
    PyPlot.close()
end
