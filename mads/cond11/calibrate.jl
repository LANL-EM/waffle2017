machinenames = ["madsmen", "madsdam", "madszem", "madskil", "madsart", "madsend"]
const numprocspermachine = 32
if nprocs() == 1
	for i = 1:numprocspermachine
		addprocs(machinenames)#do it this way so that the worker id's are not blocked onto each machine to help load balance
		println("adding procs")
	end
end
@show nprocs()
@everywhere workingdir = remotecall_fetch(1, ()->pwd())
@everywhere cd(workingdir)
@everywhere println(pwd())
@show nprocs()
import Mads
@show nprocs()
if !isdefined(:md)
	md = Mads.loadmadsfile("w01-v01.mads")
end
@show nprocs()
inverse_parameters, inverse_results = Mads.calibrate(md; np_lambda=42, lambda_mu=1.5, tolX=1e-12)
Mads.setparamsinit!(md, inverse_parameters)
Mads.savemadsfile(md)
