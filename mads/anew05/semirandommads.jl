import PyCall
@PyCall.pyimport yaml

if !isdefined(:data)
	f = open("w01-v01.mads")
	data = yaml.load(f)
	close(f)
end

srand(0)
for i = 1:1
	initkx = -5 + 2 * rand()
	initky = -5 + 2 * rand()
	initky = -6 + 2 * rand()
	inits = -4 + 2 * rand()
	for paramdict in data["Parameters"]
		paramname = collect(keys(paramdict))[1]
		paraminfo = paramdict[paramname]
		if ismatch(r"^kx\d+$", paramname)
			initval = initkx
		elseif ismatch(r"^ky\d+$", paramname)
			initval = initky
		elseif ismatch(r"^kz\d+$", paramname)
			initval = initky
		elseif ismatch(r"^s\d+$", paramname)
			initval = inits
		else
			if haskey(paraminfo, "init_min")
				initval = paraminfo["init_min"] + (paraminfo["init_max"] - paraminfo["init_min"]) * rand()
			else
				initval = paraminfo["init"]
			end
		end
		paraminfo["init"] = initval
	end
	filename = "r$i-v01.mads"
	f = open("r$i-v01.mads", "w")
	@time yaml.dump(data, f, width=255)
	close(f)
end
