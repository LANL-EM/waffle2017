import DataStructures
import PyPlot
import FEHM
import JLD
import HDF5

function scenarioplots(scenarios::Associative, wellnames::Array{String}, nodenumbers::Array{Vector{Int64}}; workingdir::AbstractString=".", case::AbstractString="waffle", plotdir::AbstractString="scenario_plots")
	if !isdir(joinpath(workingdir, plotdir))
		mkdir(joinpath(workingdir, plotdir))
	end
	wls = Dict()
	crs = Dict()
	times = Dict()
	scenarionames = sort(collect(keys(scenarios)))
	for scenarioname in scenarionames
		info("Scenario: $scenarioname")
		if !isfile("$workingdir/w01_$(scenarioname).jld")
			warn("JLD file $workingdir/w01_$(scenarioname).jld is missing!")
		else
			fh5 = HDF5.h5open("$workingdir/w01_$(scenarioname).jld")
			kh5 = HDF5.names(fh5)
			HDF5.close(fh5)
			timedata = nothing
			wldata = nothing
			crdata = nothing
			any(kh5.=="times") && (timedata = JLD.load("$workingdir/w01_$(scenarioname).jld", "times"))
			any(kh5.=="Cr") && (crdata = JLD.load("$workingdir/w01_$(scenarioname).jld", "Cr"))
			any(kh5.=="WL") && (wldata = JLD.load("$workingdir/w01_$(scenarioname).jld", "WL"))
			times[scenarioname] = FEHM.fehmday2year.(timedata)
			if wldata != nothing
				wls[scenarioname] = Dict()
				for i = 1:length(wellnames)
					wls[scenarioname][wellnames[i]] = map(t->mean(wldata[t][nodenumbers[i]]), 1:length(crdata))
					# @show scenarioname, wellnames[i], maximum(wls[scenarioname][wellnames[i]])
				end
			end
			if crdata != nothing
				crs[scenarioname] = Dict()
				for i = 1:length(wellnames)
					crs[scenarioname][wellnames[i]] = map(t->mean(crdata[t][nodenumbers[i]]), 1:length(crdata))
					# @show scenarioname, wellnames[i], maximum(crs[scenarioname][wellnames[i]])
				end
			end
		end
	end
	if !isdir(plotdir)
		mkdir(plotdir)
	end
	for i = 1:length(wellnames)
		plotwaterlevelcurves(wellnames[i], scenarionames, times, wls; case=case, plotdir=joinpath(workingdir, plotdir))
		plotbreakthroughcurves(wellnames[i], scenarionames, times, crs; case=case, plotdir=joinpath(workingdir, plotdir))
	end
end

function scenarioplots(scenarios::Associative; case::AbstractString="waffle", plotdir::AbstractString="scenario_plots", wells::Array{String}=[["crin$i" for i in 1:6]; ["crin6e", "crin6w", "crex1","crex2","crex3","crpz1","crpz2_1","crpz3","crpz4","crpz5","r11","r13","r15","r28","r33_1","r33_2","r35b","r36","r42","r43_1","r43_2","r44_1","r44_2","r45_1","r45_2","r50_1","r50_2","r61_1","r61_2","r62","simr2"]])
	wellname2zone = DataStructures.OrderedDict(zip(calibwells, calibzones))
	znfilename = joinpath(workingdir, "..", "..", "smoothgrid2", "well_screens.zonn")
	node2zone, zone2nodes = FEHM.parsezone(znfilename; returndict=true)
	wls = Dict()
	crs = Dict()
	wltimes = Dict()
	crtimes = Dict()
	scenarionames = sort(collect(keys(scenarios)))
	for scenarioname in scenarionames
		answer = postprocess_full(Symbol("wl_" * scenarioname), wellname2zone, node2zone, zone2nodes)
		if answer != nothing
			wltimes[scenarioname], wls[scenarioname], crtimes[scenarioname], crs[scenarioname] = answer
		end
	end
	if !isdir(plotdir)
		mkdir(plotdir)
	end
	scenarionames = sort(collect(keys(wltimes)))
	for w = wells
		plotwaterlevelcurves(w, scenarionames, wltimes, wls; case=case, plotdir=plotdir)
		plotbreakthroughcurves(w, scenarionames, crtimes, crs; case=case, plotdir=plotdir)
	end
end

function plotbreakthroughcurves(wellname::AbstractString, scenarionames::Vector, crtimes::Associative, crs::Associative; case::AbstractString="waffle", plotdir::AbstractString=".", range::Vector = collect(2005:2027))
	crfig, crax = PyPlot.subplots()
	crfig[:set_size_inches](8, 5)
	maxobs = 0.
	for scenarioname in scenarionames
		if haskey(crs, scenarioname)
			crax[:plot](crtimes[scenarioname], crs[scenarioname][wellname], scenarios[scenarioname]["color"], linewidth=3, alpha=0.5)
			# @show wellname, scenarioname, maximum(crs[scenarioname][wellname])
			maxobs = max(maxobs, maximum(crs[scenarioname][wellname]))
		end
	end
	crax[:plot](range, map(x->50, range), "k--", linewidth=3, label="_nolegend_")
	crax[:set_title](wellname)
	crax[:set_xlabel]("Year")
	crax[:set_ylabel]("Concentration [ppb]")
	if maxobs < 50
		crax[:set_ylim](0, 60)
	end
	crax[:set_xlim](range[1], range[end])
	crax[:legend](map(x->(haskey(crs, x) && scenarios[x]["legend"]), scenarionames), loc=6)
	crfig[:tight_layout]()
	crfig[:savefig](string("$(plotdir)/$(case)_btcs_", wellname, ".pdf"))
	PyPlot.close(crfig)
end

function plotwaterlevelcurves(wellname::AbstractString, scenarionames::Vector, wltimes::Associative, wls::Associative; case::AbstractString="waffle", plotdir::AbstractString=".")
	wlfig, wlax = PyPlot.subplots()
	wlfig[:set_size_inches](8, 5)
	maxobs = 0.
	for scenarioname in scenarionames
		if haskey(wls, scenarioname)
			wlax[:plot](wltimes[scenarioname], wls[scenarioname][wellname], scenarios[scenarioname]["color"], linewidth=3, alpha=0.5)
		end
	end
	wlax[:set_title](wellname)
	wlax[:set_xlabel]("Year")
	wlax[:set_ylabel]("Water level [m]")
	wlax[:set_xlim](2016.9, 2018)
	wlax[:legend](map(x->(haskey(wls, x) && scenarios[x]["legend"]), scenarionames))
	wlfig[:tight_layout]()
	wlfig[:savefig](string("$(plotdir)/$(case)_wls_", wellname, ".pdf"))
	PyPlot.close(wlfig)
end
