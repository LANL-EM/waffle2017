import JLD
import Glob
import JLD2
import FileIO

@everywhere function convert(name::AbstractString)
    out = split(name, ".")[1]*".jld2"
    if !isfile(out)
        info("Loading $(name)")
        Cr,WL,times = FileIO.load(name, "Cr", "WL", "times")
        info("Writting $(out)")
        FileIO.save(out, "Cr", Cr, "WL", WL, "times", times) 
        info("Complete $(out)")
    end
end
names = Glob.glob("cond11_chain_*.jld")
pmap(convert, names)
