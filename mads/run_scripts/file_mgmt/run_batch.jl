import Mads 

@everywhere function setup_args_and_run(index::Int64)
    # Runs FEHM for chain values 
    startingdir = pwd()
    cd("/lclscratch/jhyman/waffle2017/mads")
    madsfile = "$(base)_$(index)/$(base)_$(index).mads"
    gridfile = "../../../smoothgrid2/tet.geo"
    info("Mads file: $(madsfile)")
    info("Grid file: $(gridfile)")
    #@time run(`/home/jhyman/codes/julia-0d7248e2ff/bin/julia remediationscenarios.jl $(base)_$(index)/$(base)_$(index).mads ../../../smoothgrid2/tet.geo > $(base)_$(index)/fehmrun_output.txt`)
    @time run(`julia remediationscenarios.jl $(base)_$(index)/$(base)_$(index).mads ../../../smoothgrid2/tet.geo > $(base)_$(index)/fehmrun_output.txt`)
    cd(startingdir)
    info("$(base)_$(index) is complete")
end

@everywhere base = "cond11_chain"

runs = [1]

println("Running $(length(runs)) Simulations")
println(runs)

startingdir = pwd()
for index in runs
    println("Building dir for run $index")
    tempdir = "../$(base)_$(index)"
    Mads.createtempdir(tempdir)
    Mads.linktempdir(startingdir, tempdir) 
end

println("Loading Chain")
chain, llhoods = JLD.load("chain.jld", "chain")
println("Loading Chain Complete")
thinchain = chain[:,runs]
println("Loading Mads file")
if !isdefined(:md)
    md = Mads.loadmadsfile("w01-v01.mads")
end
println("Loading Mads file complete")
paramkeys = Mads.getoptparamkeys(md)

for (i,index) in enumerate(runs)
    if !isfile("../$(base)_$index/$(base)_$index.mads")
        println("Dumping mads file for run $index") 
        Mads.setparamsinit!(md, Dict(zip(paramkeys, thinchain[:, i])))
        println("Writting new mads file")
        @time Mads.savemadsfile(md, "../$(base)_$index/$(base)_$index.mads"; observations_separate=true, filenameobs="../$(base)_1/$(base)_1-observations.yaml")
        println("Writting new mads file complete")
    end
end

ii = pmap(setup_args_and_run,runs)
