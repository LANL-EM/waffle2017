import JLD
using StatsBase

chain, llhoods  = JLD.load("chain.jld", "chain")
hash_vals = []
chain_length = 16000
for i in 1:chain_length 
    push!(hash_vals,hash(chain[:,i]))
end
println(unique(hash_vals))
u = Set(unique(hash_vals))
d = Dict([(i,count(x->x==i,hash_vals)) for i in u])
println(d)

chain_vals = []
for i in 1:chain_length
    h = hash(chain[:,i])
    if h in u 
        push!(chain_vals, i)
        delete!(u, h)
    end 
end
println(chain_vals)
JLD.save("unique_chain.jld", "index", chain_vals)

chain_cnts = zeros(Float64, length(u)) 
chain_index = -1*ones(Int64, length(u)) 

for i in 1:chain_length
    if i % 1000 == 0
        println(i)
    end
    tmp = hash(chain[:,i])
    for (j,v) in enumerate(u)
        if v == tmp 
            chain_cnts[j] += 1
            if chain_index[j] < 0
                chain_index[j] = i
            end
            break 
        end
    end
end

total=16000.0
chain_cnts = chain_cnts/total

for i in 1:length(u)
    println("Chain $(chain_index[i]) is repeated $(chain_cnts[i]) times")
end

JLD.save("chain_cnts.jld", "cnt", chain_cnts)

# With replacement
my_samps = sample(chain_index, Weights(chain_cnts),   400)
my_samps = sort(my_samps)
println(my_samps)

for i in 1:length(my_samps)
    println("Chain $(my_samps[i]) is repeated $(chain_cnts[i]) times")
end

JLD.save("sub_chain_full.jld", "index", my_samps)
println(length(unique((my_samps))
JLD.save("sub_chain_thin.jld", "index", unique(my_samps))



