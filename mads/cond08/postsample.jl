import JLD
@everywhere workingdir = remotecall_fetch(1, ()->pwd())
@everywhere cd(workingdir)
@everywhere println(pwd())
@show nprocs()
import Mads
@show nprocs()
if !isdefined(:md)
	md = Mads.loadmadsfile("w01-v01.mads")
end
@show nprocs()
chain, llhoods = JLD.load("chain_20170418.jld", "chain")
bestindex = indmax(llhoods)
for (i, k) in enumerate(Mads.getoptparamkeys(md))
	md["Parameters"][k]["init"] = chain[i, bestindex]
end
result = Mads.forward(md)
include("plotresults.jl")
