import PyCall
@PyCall.pyimport aquiferdb as db

include("pilotpointdata.jl")

anisotropydata = readdlm("../../data/vertical_anisotropy.dat"; comments=false)
anisotropy = Dict()
for i = 1:size(anisotropydata, 1)
	if anisotropydata[i, 4] != "ND"
		anisotropy[anisotropydata[i, 1]] = anisotropydata[i, 4]
		anisotropy[string(anisotropydata[i, 1], "#1")] = anisotropydata[i, 4]
		anisotropy[string(anisotropydata[i, 1], "#2")] = anisotropydata[i, 4]
		anisotropy[string(anisotropydata[i, 1], "a")] = anisotropydata[i, 4]
		anisotropy[string(anisotropydata[i, 1], "b")] = anisotropydata[i, 4]
	end
end

hycodata = readdlm("../../data/hyco_meters_per_day.dat"; comments=false)
hycometerspersecond = Array(Float64, size(hycodata, 1))
hycolocations = Array(Float64, size(hycodata, 1), 3)
hyconames = Array(String, size(hycodata, 1))
hycoanisotropy = Array(Any, size(hycodata, 1))
for i = 1:size(hycodata, 1)
	hyconames[i] = hycodata[i, 1]
	hycometerspersecond[i] = hycodata[i, 6] / (24 * 3600)
	hycolocations[i, 1] = hycodata[i, 2]
	hycolocations[i, 2] = hycodata[i, 3]
	hycolocations[i, 3] = .5 * (hycodata[i, 4] + hycodata[i, 5])
	if hyconames[i] in keys(anisotropy)
		hycoanisotropy[i] = anisotropy[hyconames[i]]
	else
		hycoanisotropy[i] = false
	end
end

wells = unique(vcat(map(x->x[1], logicalpilotpoints)...))
wellcoords = Dict()
db.connecttodb()
for well in wells
	x, y, r = db.getgeometry(well)
	wellcoords[well] = [x, y]
end
db.disconnectfromdb()
function getpilotpointcoords(logicalpilotpoint)
	wellnames = logicalpilotpoint[1]
	elevation = logicalpilotpoint[2]
	return [mean(map(x->wellcoords[x], wellnames)); [elevation]]
end
pilotpointcoordss = map(getpilotpointcoords, logicalpilotpoints)

function tooclose(coords)
	for i = 1:size(hycolocations, 1)
		if norm(coords - hycolocations[i, :]) < 25
			return hyconames[i], norm(coords - hycolocations[i, :])
		end
	end
	return false
end
goodppindices = fill(true, length(pilotpointcoordss))
goodpps = Array{Float64, 1}[]
for i = 1:length(pilotpointcoordss)
	tcval = tooclose(pilotpointcoordss[i])
	if tcval != false
		#println(string(logicalpilotpoints[i], " is too close to ", tcval))
		goodppindices[i] = false
	else
		push!(goodpps, pilotpointcoordss[i])
		#println(string(logicalpilotpoints[i], " is fine"))
	end
end

writedlm("pilotpoints.txt", vcat(hycolocations, map(x->x', goodpps)...))
fhyco = open("w01.tpl_hyco_hetero", "w")
write(fhyco, "template #\n")
write(fhyco, join(map(i->"#kx$i#", 1:length(hyconames)), " "))
write(fhyco, " ")
write(fhyco, join(map(i->"#kx$i#", length(hyconames) + 1:length(hyconames) + sum(goodppindices)), " "))
write(fhyco, " ")
write(fhyco, join(map(i->"#ky$i#", 1:length(hyconames)), " "))
write(fhyco, " ")
write(fhyco, join(map(i->"#ky$i#", length(hyconames) + 1:length(hyconames) + sum(goodppindices)), " "))
write(fhyco, " ")
write(fhyco, join(map(i->"#kz$i#", 1:length(hyconames)), " "))
write(fhyco, " ")
write(fhyco, join(map(i->"#kz$i#", length(hyconames) + 1:length(hyconames) + sum(goodppindices)), " "))
close(fhyco)
fstor = open("w01.tpl_stor_hetero", "w")
write(fstor, "template #\n")
write(fstor, join(map(i->"#s$i#", 1:length(hyconames)), " "))
write(fstor, " ")
write(fstor, join(map(i->"#s$i#", length(hyconames) + 1:length(hyconames) + sum(goodppindices)), " "))
close(fstor)

for i = 1:length(hyconames)
	if contains(hyconames[i], "PM-") || contains(hyconames[i], "O-")
		scalingfactor = 10
	else
		scalingfactor = 2
	end
	println("- kx$i: { longname: \"kx_$(hyconames[i])\", init: $(log10(hycometerspersecond[i])), type: opt, log: no, min: $(log10(hycometerspersecond[i]) - log10(scalingfactor)), max: $(log10(hycometerspersecond[i]) + log10(scalingfactor)) }")
	println("- ky$i: { longname: \"ky_$(hyconames[i])\", init: $(log10(hycometerspersecond[i])), type: opt, log: no, min: $(log10(hycometerspersecond[i]) - log10(scalingfactor)), max: $(log10(hycometerspersecond[i]) + log10(scalingfactor)) }")
	if hycoanisotropy[i] == false
		kz = 1e-6
	else
		kz = max(1e-9, hycometerspersecond[i] * hycoanisotropy[i])
	end
	println("- kz$i: { longname: \"kz_$(hyconames[i])\", init: $(log10(kz)), type: opt, log: no, min: $(log10(kz) - log10(scalingfactor)), max: $(log10(kz) + log10(scalingfactor)), init_min: $(log10(kz) - log10(scalingfactor)), init_max: $(log10(kz) + log10(scalingfactor)) }")
	println("- s$i: { longname: \"s_$(hyconames[i])\", init: -3, type: opt, log: no, min: -8, max: -1, init_min: -4.0, init_max: -2.0 }")
end
n = length(hyconames) + 1
for i = 1:length(pilotpointcoordss)
	if goodppindices[i]
		name = replace(string(logicalpilotpoints[i])[8:end], "\"", "")
		println("- kx$n: { longname: \"kx_($(name)\", init: -4.7, type: opt, log: no, min: -8, max: -2, init_min: -5.7, init_max: -3.7 }")
		println("- ky$n: { longname: \"ky_($(name)\", init: -4.7, type: opt, log: no, min: -8, max: -2, init_min: -5.7, init_max: -3.7 }")
		println("- kz$n: { longname: \"kz_($(name)\", init: -6, type: opt, log: no, min: -9, max: -3, init_min: -7.0, init_max: -5.0 }")
		println("- s$n: { longname: \"s_($(name)\", init: -3, type: opt, log: no, min: -8, max: -1, init_min: -4.0, init_max: -2.0 }")
		n += 1
	end
end
