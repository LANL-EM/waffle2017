import FEHM

function postprocess_full(dir::Symbol, wellname2zone::Associative, node2zone::Associative, zone2nodes::Associative; wlfilename::AbstractString="w01_head_his.dat", crfilename::AbstractString="w01_Cr.dat")
	dir = string(dir)
	info("Postprocessing FEHM outputs in $(string(dir)) ...")
	lineswl = FEHM.myreadlines(joinpath(dir, wlfilename))
	linescr = FEHM.myreadlines(joinpath(dir, crfilename))
	if lineswl == nothing || linescr == nothing
		return nothing
	end
	nodeheaders = map(x -> parse(Int, x[2:8]), split(lineswl[2], "Node")[2:end])
	timesandwls = map(line -> map(float, split(line, " "; limit=0, keep=false)[1:end-1]), lineswl[11:end])
	timesandcrs = map(line -> map(float, split(line, " "; limit=0, keep=false)[1:end-1]), linescr[11:end])
	wltimes = map(x->x[1], timesandwls) / 365.25 + modelstartyear
	crtimes = map(x->x[1], timesandcrs) / 365.25 + modelstartyear
	wls = Dict()
	crs = Dict()
	for wn in keys(wellname2zone)
		wls[wn] = zeros(length(wltimes))
		crs[wn] = zeros(length(crtimes))
	end
	#find the columns that corresponds to the zones we want to calibrate against
	for wn in keys(wellname2zone)
		goodnodes = zone2nodes[parse(Int, wellname2zone[wn])]
		numfoundnodes = 0
		for j = 1:length(nodeheaders)
			if nodeheaders[j] in goodnodes
				numfoundnodes += 1
				wls[wn] += map(x->x[j + 1], timesandwls)
				crs[wn] += map(x->x[j + 1], timesandcrs)
			end
		end
		@assert numfoundnodes == length(goodnodes)
		wls[wn] /= length(goodnodes)
		crs[wn] /= length(goodnodes)
	end
	return wltimes, wls, crtimes, crs
end