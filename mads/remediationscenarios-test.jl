import PyPlot
#import YAML
#import RobustPmap
#import Mads

zemdir = Base.source_path()
if zemdir == nothing
	zemdir = splitdir(dirname(@__FILE__))[1]
else
	zemdir = splitdir(zemdir)[1]
end

@everywhere modelstartyear = 1964
@everywhere include("setscenarios.jl")
@everywhere include("newwellzones.jl")
@everywhere include("runscenario.jl")
@everywhere include("postprocss_full.jl")
@everywhere include("postprocessfehm.jl")
@everywhere include("scenarioplots.jl")
include("scenariomovies.jl")
include("dummywells.jl")

if length(ARGS) == 0
	defaultmadsfilename = "cond11/w01-v10.mads"
	push!(ARGS, defaultmadsfilename)
	warn("Using default Mads file: $defaultmadsfilename")
end
if length(ARGS) < 2
	defaultgeofilename = "../../../smoothgrid2/tet.geo"
	push!(ARGS, defaultgeofilename)
	warn("Using default grid geometry file: $defaultgeofilename")
end
if !isfile(ARGS[1])
	error("Mads file $(ARGS[1]) is missing!")
end
info("Mads file: $(ARGS[1])")
info("Grid file: $(ARGS[2])")

sp = splitdir(ARGS[1])
case = split(sp[end-1], '_')[1]
info("Case: $(case)")
workingdir = abspath(joinpath(sp[1:end - 1]...))
madsfilename = sp[end]
geofilename = ARGS[2]
scenarios = setscenarios()
@eval @everywhere scenarios = $scenarios
@eval @everywhere geofilename = $geofilename
@eval @everywhere workingdir = $workingdir
@eval @everywhere case = $case

@everywhere startdir = pwd()
@everywhere cd(workingdir)

#=
if !isdefined(:md)
	info("Loading Mads input file ..")
	md = Mads.loadmadsfile(madsfilename)
end
Mads.writeparameters(md)

include(abspath("gettrends.jl"))
include(abspath("runmodelcode.jl"))

push!(calibwells, "simr2")
push!(calibzones, "7050201")
starttimedict["7050201"] = 365
crtimedict["7050201"] = collect(2005:2027)
push!(calibwells, "crin6e")
push!(calibzones, "610603")
starttimedict["610603"] = 365
crtimedict["610603"] = collect(2005:2027)
push!(calibwells, "crin6w")
push!(calibzones, "610602")
starttimedict["610602"] = 365
crtimedict["610602"] = collect(2005:2027)
for k in keys(crtimedict)
	crtimedict[k] = collect(2005:2027)
end

if isfile("w01.hyco")
	warn("Skipping main directory setup")
else
	info("Setting-up the main directory ... ")
	setupmaindir()
end
if isdir("wl")
	warn("Skipping preprocessing setup of directory `wl`")
else
	info("Preprocessing setup of directory `wl` ...")
	preprocess(:wl)
end

info("Performing FEHM runs ...")
RobustPmap.rpmap(x->runscenario(x, scenarios[x]["wellsinuse"], scenarios[x]["pntstartyear"], scenarios[x]["gpmrates"], scenarios[x]["pumptimes"]), keys(scenarios))
#map(x->runscenario(x, scenarios[x]["wellsinuse"], scenarios[x]["pntstartyear"], scenarios[x]["gpmrates"], scenarios[x]["duration"]), keys(scenarios))

info("Post processing FEHM runs ...")
postprocessfehm(scenarios)
info("Plotting scenarios curves ...")
scenarioplots(scenarios; case=case)
info("Setting dummy wells ...")
wellnames, zonenumbers, nodenumbers, coordinates = dummywells()
info("Plotting scenarios curves for dummy wells ...")
scenarioplots(scenarios, wellnames, nodenumbers; case=case)
info("Making scenarios movies ...")
include("scenariomovies.jl")
=#
scenariomovies(scenarios; case=case)

