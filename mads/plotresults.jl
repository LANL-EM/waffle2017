import Mads
import Gadfly
import DataStructures
import DataFrames
import PyCall
@PyCall.pyimport aquiferdb as db
db.connecttodb()
import PyPlot

tickfontsize = 16
labelfontsize = 18

if !isdefined(:md)
	@time md = Mads.loadmadsfile("w01-06-v09.mads")
end
if !isdefined(:result)
	result = Mads.forward(md)
end
k2t(s)=float(split(s, "_")[end])
function k2well(s)
	s = join(split(s, "_")[2:end], " #")
	return wellname(s)
	#=
	if startswith(s, "crpz")
		s = string("CrPZ-", s[5:end])
	elseif startswith(s, "crex")
		s = string("CrEX-", s[5:end])
	elseif startswith(s, "crin")
		s = string("CrIN-", s[5:end])
	elseif startswith(s, "r")
		s = string("R-", s[2:end])
	end
	return s
	=#
end
ndday2date(i)=Dates.Date(2012, 1, 1) + i * (Date(1899, 12, 30) - Date(1899, 12, 29))
function k2date(s)
	ndday2date(parse(Int, split(s, "_")[1][2:end]))
end
function wellname(s)
	if startswith(s, "crpz")
		s = string("CrPZ-", s[5:end])
	elseif startswith(s, "crex")
		s = string("CrEX-", s[5:end])
	elseif startswith(s, "crin")
		s = string("CrIN-", s[5:end])
	elseif startswith(s, "r")
		s = string("R-", s[2:end])
	elseif startswith(s, "pm")
		s = string("PM-", s[3:end])
	elseif startswith(s, "o")
		s = string("O-", s[2:end])
	end
	return s
end
function fullwellname(s)
	return join(split(wellname(s), "_")[1:end], " #")
end
function shortwellname(s)
	return split(wellname(s), "_")[1]
end
portdesc(well) = length(split(well, "_")) == 1 ? "SINGLE COMPLETION" : split(well, "_")[2]

#=
obswells = ["r11", "r13", "r15", "r28", "r33_1", "r33_2", "r35b", "r36", "r42", "r43_1", "r43_2", "r44_1", "r44_2", "r45_1", "r45_2", "r50_1", "r50_2", "r61_1", "r61_2", "r62"]
pumpingwells = ["pm01", "pm02", "pm03", "pm04", "pm05", "o04", "r28", "r42", "crex1"]
=#
obswells = ["crex1","crex3","crpz1","crpz2_1","crpz3","crpz4","crpz5","r1","r11","r13","r15","r28","r33_1","r33_2","r35a","r35b","r36","r42","r43_1","r43_2","r44_1","r44_2","r45_1","r45_2","r50_1","r50_2","r61_1","r61_2","r62","simr2"]
pumpingwells = ["o04","crin3","crin2","pm05","pm04","pm02","crin5","crin4","crex1","r28","crex3","r42","crin1"]
fig, ax = PyPlot.subplots(figsize=(16, 9))
wlkeys = Mads.filterkeys(result, Regex("wl_"))
wlkeys = filter(x->md["Observations"][x]["weight"] > 0, wlkeys)
ax[:plot](1:length(wlkeys), map(x->result[x], wlkeys), "k", linewidth=3)
ax[:plot](1:length(wlkeys), map(x->md["Observations"][x]["target"], wlkeys), "r.", markersize=10)
ax[:set_xticks](1:length(wlkeys))
ax[:set_xticklabels](k2well.(wlkeys), rotation="vertical")
ax[:set_ylabel]("Water Level [m]", fontsize=labelfontsize)
ax[:tick_params](labelsize=tickfontsize)
fig[:tight_layout]()
fig[:savefig]("figs/wls.pdf")
display(fig); println()
PyPlot.close(fig)

for well in obswells
	for pwell in pumpingwells
		keys = Mads.filterkeys(result, Regex("P[0-9]+_$(well)_$(pwell)"))
		posweightkeys = filter(x->md["Observations"][x]["weight"] > 0, keys)
		if length(posweightkeys) > 0
			fig, ax = PyPlot.subplots(figsize=(16, 9))
			ax[:plot](k2date.(keys), map(x->result[x], keys), "k", linewidth=3)
			ax[:plot](k2date.(posweightkeys), map(x->md["Observations"][x]["target"], posweightkeys), "r.", markersize=10)
			ax[:set_ylabel]("$(fullwellname(well)) drawdown from $(fullwellname(pwell)) [m]", fontsize=labelfontsize)
			ax[:tick_params](labelsize=tickfontsize)
			fig[:autofmt_xdate]()
			fig[:tight_layout]()
			display(fig); println()
			fig[:savefig]("figs/dd_$(well)_$(pwell).pdf")
			PyPlot.close(fig)
		else
			warn("no calibration targets for $well/$pwell")
		end
	end
end
#plot the concentrations including all the observations
for well in obswells
	concobs, obstimes = db.getchromiumconcentrations(shortwellname(well), portdesc(well))
	if length(obstimes) == 0
		concobs, obstimes = db.getchromiumconcentrations(shortwellname(well), portdesc(well), fullchemdb=true, filterresults=false)
	end
	obsnames = collect(filter(x->ismatch(Regex("conc_$(well)_[0-9]+"), x), keys(md["Observations"])))
	concpreds = Float64[]
	predtimes = Any[]
	for obsname in obsnames
		push!(concpreds, result[obsname])
		push!(predtimes, DateTime(k2t(obsname)))
	end
	if length(concpreds) > 0
		fig, ax = PyPlot.subplots(figsize=(16, 9))
		if length(obstimes) > 0
			ax[:plot](obstimes, concobs, "r.", markersize=10)
		end
		ax[:plot](predtimes, concpreds, "k", linewidth=3)
		ax[:set_ylabel]("$(fullwellname(well)) Cr [ppb]", fontsize=labelfontsize)
		ax[:tick_params](labelsize=tickfontsize)
		fig[:autofmt_xdate]()
		fig[:tight_layout]()
		display(fig); println()
		fig[:savefig]("figs/totalconc_$(well).pdf")
		PyPlot.close(fig)
	end
end
#plot the drawdown for all the pumping wells
for owell in obswells
	totaldrawdown = DataStructures.DefaultDict(0.)
	totaldrawdowntarget = DataStructures.DefaultDict(0.)
	obsnames = collect(filter(x->ismatch(Regex("P\\d+_$(owell)_"), x), keys(md["Observations"])))
	obsnames = collect(filter(x->!contains(x, "crex1"), obsnames))
	for obsname in obsnames
		if md["Observations"][obsname]["weight"] > 0 || owell == "r61_1" || owell == "r61_2"
			totaldrawdown[k2date(obsname)] += result[obsname]
			totaldrawdowntarget[k2date(obsname)] += md["Observations"][obsname]["target"]
		end
	end
	if length(totaldrawdown) > 0
		fig, ax = PyPlot.subplots(figsize=(16, 9))
		sortedkeys = sort(collect(keys(totaldrawdown)))
		ax[:plot](sortedkeys, map(x->totaldrawdown[x], sortedkeys), "k", linewidth=3)
		sortedkeys = sort(collect(keys(totaldrawdowntarget)))
		sortedkeys = filter(x->totaldrawdowntarget[x] != 0.0, sortedkeys)
		ax[:plot](sortedkeys, map(x->totaldrawdowntarget[x], sortedkeys), "r.", markersize=10)
		ax[:set_ylabel]("$(fullwellname(owell)) drawdown [m]", fontsize=labelfontsize)
		ax[:tick_params](labelsize=tickfontsize)
		fig[:autofmt_xdate]()
		fig[:tight_layout]()
		display(fig); println()
		fig[:savefig]("figs/totaldd_$(owell).pdf")
		PyPlot.close(fig)
	else
		warn("no drawdowns for $owell")
	end
end
for owell in obswells
	info(string("OF from concs at $owell (points, trend): ", Mads.partialof(md, result, Regex("conc_$(owell)_[0-9]+")), ", ", Mads.partialof(md, result, Regex("conc_$(owell)_slope"))))
end
for pwell in pumpingwells
	info(string("OF from pumping at $pwell: ", Mads.partialof(md, result, Regex("P.*_$pwell"))))
end
info(string("OF from wls: ", Mads.partialof(md, result, Regex("wl_"))))
info(string("OF from concentration trends: ", Mads.partialof(md, result, Regex("_slope"))))
db.disconnectfromdb()
