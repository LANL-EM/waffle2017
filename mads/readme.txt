anew01: copy of sd10.jl from model2014waffle, but adds the crin/crex calibration targets
anew02: like sd08b.jl, but adds the crin/crex calibration targets
crazy01: adds conditioning points based on 24 hour pumping tests
crazy02: like crazy01, but add init_min and init_max
cond03: like crazy02, but fixes the pilot/conditioning point craziness, and fixes an issue with "opt" params in the mads file
cond04: copy of cond03 start point, but adds the calibrated source from crazy01
cond05: picks up where cond03 left off, but makes PM-04 permeability an "opt" parameter
cond06: like cond03, but make PM-04 permeability an opt parameter
cond07: like cond03, but adds wiggle room around the conditioning points (factor of 10 at the supply wells, factor of 2 elsewhere), and fixes some issues with the init_min/max's
cond08: starts where cond05 left off, but adds wiggle room (factor of 2 in kx,ky at monitoring wells, factor of 10 in kz at monitoring wells, factor of 10 in kx,ky at supply wells)
cond05a: picks up where cond05 left off, but adds 2016 observations for crpz1, crpz4, crpz5
cond09: copy of cond03, but adds 2016 observations for crpz1, crpz4, and crpz5
cond10: picks up where cond09 left off, but fixes the weights for the crin/crpz Cr observations
cond11: starts where cond09 started, but fixes the weights for the crin/crpz Cr observations (like cond10, but starts where cond09 started instead of where cond09 finished)
sample01: copy of cond10 for sampling purposes
sample02: sampling run that starts where cond11 finished
nd01: first attempt at incorporating the new theis data
nd02: more work on new data
nd03: picks up where nd02 left off, but sets weights to zero for drawdowns at r35a and r33#2
