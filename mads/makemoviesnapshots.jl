import CrPlots

function makemoviesnapshots(xs::Vector, ys::Vector, times::Vector, data::Vector, topnodes::Vector, lowerlimit::Number, upperlimit::Number, rootfilenameshapshot::AbstractString, rootfilenamemovie::AbstractString, boundingbox::Tuple, plotwells::Vector; cmap=CrPlots.rainbow, infoouput::AbstractString="Snapshot", legendtitle::AbstractString="Cr [ppb]")
	if isfile("$rootfilenamemovie.mp4")
		warn("Movie file $rootfilenamemovie.mp4 already exist! Skipping!")
		return
	end
	@sync @parallel for (i, t) in collect(enumerate(times))
		plotyear = 1964 + (t / 365.25)
		filename = "$rootfilenameshapshot$(lpad(i, 4, 0)).png"
		info("$infoouput $plotyear: $filename")
		fig, ax, img = CrPlots.crplot(boundingbox, xs[topnodes], ys[topnodes], data[i]; upperlimit=upperlimit, lowerlimit=lowerlimit, cmap=CrPlots.rainbow)
		CrPlots.addwells(ax, plotwells)
		CrPlots.addcbar(fig, img, legendtitle, CrPlots.getticks([lowerlimit, upperlimit]), lowerlimit, upperlimit; cbar_y0=0.02)
		CrPlots.addmeter(ax, boundingbox[3] - 1050, boundingbox[2] - 50, [250, 500, 1000], ["250 m", "500 m", "1 km"])
		CrPlots.addpbar(fig, ax, (t - minimum(times)) / (maximum(times) - minimum(times)), "Year $(@sprintf "%.2f" plotyear)")
		#display(fig); println()
		fig[:savefig](filename, dpi=120)
		PyPlot.close(fig)
	end
	run(`ffmpeg -i $rootfilenameshapshot%04d.png -vcodec libx264 -pix_fmt yuv420p -f mp4 -y $rootfilenamemovie.mp4`)
end

function makemoviesnapshots(xs::Vector, ys::Vector, times::Vector, data::Vector, topnodes::Vector, lowerlimit::Number, upperlimit::Number, blanklimit::Number, rootfilenameshapshot::AbstractString, rootfilenamemovie::AbstractString, boundingbox::Tuple, plotwells::Vector; cmap=CrPlots.rainbow, infoouput::AbstractString="Snapshot", legendtitle::AbstractString="Cr [ppb]")
	if isfile("$rootfilenamemovie.mp4")
		warn("Movie file $rootfilenamemovie.mp4 already exist! Skipping!")
		return
	end
	@sync @parallel for (i, t) in collect(enumerate(times))
		plotyear = 1964 + (t / 365.25)
		filename = "$rootfilenameshapshot$(lpad(i, 4, 0)).png"
		info("$infoouput $plotyear: $filename")
		badindices = find(i->i.<blanklimit, data[i])
		dc = copy(data[i])
		dc[badindices] .= 0
		fig, ax, img = CrPlots.crplot(boundingbox, xs[topnodes], ys[topnodes], dc; upperlimit=upperlimit, lowerlimit=blanklimit, cmap=CrPlots.redwhite, alpha=0.7)
		CrPlots.addcbar(fig, img, legendtitle, CrPlots.getticks(blanklimit, upperlimit), blanklimit, upperlimit; cbar_y0=0.25, cbar_height=0.2, alpha=0.7)
		badindices = find(i->i.>-blanklimit, data[i])
		dc = copy(data[i])
		dc[badindices] .= 0
		fig, ax, img = CrPlots.crplot(boundingbox, xs[topnodes], ys[topnodes], dc; upperlimit=-blanklimit, lowerlimit=lowerlimit, cmap=CrPlots.whitegreen, figax=(fig, ax), alpha=0.7)
		CrPlots.addwells(ax, plotwells)
		CrPlots.addcbar(fig, img, "", CrPlots.getticks(lowerlimit, -blanklimit), lowerlimit, -blanklimit; cbar_y0=0.02, cbar_height=0.2, alpha=0.7)
		CrPlots.addmeter(ax, boundingbox[3] - 1050, boundingbox[2] - 50, [250, 500, 1000], ["250 m", "500 m", "1 km"])
		CrPlots.addpbar(fig, ax, (t - minimum(times)) / (maximum(times) - minimum(times)), "Year $(@sprintf "%.2f" plotyear)")
		#display(fig); println()
		fig[:savefig](filename, dpi=120)
		PyPlot.close(fig)
	end
	run(`ffmpeg -i $rootfilenameshapshot%04d.png -vcodec libx264 -pix_fmt yuv420p -f mp4 -y $rootfilenamemovie.mp4`)
end

function gettop13mdata(xs::Vector, ys::Vector, zs::Vector, times::Vector, topnodes::Vector, crdata::Vector)
	topdata = Array{Any}(length(times))
	for i = 1:length(times)
		topdata[i] = crdata[i][topnodes]
	end
	topdict = Dict()
	#set up the data for the average over the top 10 meters of the water table
	thirteenmeternodes = Array{Array{Int, 1}}(length(xs))
	for node in topnodes
		topdict[(xs[node], ys[node])] = node
		thirteenmeternodes[node] = Int[]
	end
	for (i, x, y, z) in zip(1:length(xs), xs, ys, zs)
		topnode = topdict[(x, y)]
		if z >= zs[topnode] - 13
			push!(thirteenmeternodes[topnode], i)
		end
	end
	thirteenmeterdata = deepcopy(topdata)
	for i in 1:length(times)
		for (j, topnode) in enumerate(topnodes)
			thirteenmeterdata[i][j] = mean(crdata[i][thirteenmeternodes[topnode]])
		end
	end
	return topdata, thirteenmeterdata
end