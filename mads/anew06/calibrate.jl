#machines = [fill("madsmax", 10); fill("madsmen", 10); fill("madsdam", 10); fill("madskil", 10); fill("madszem", 10); fill("madsart", 10); fill("madsend", 10)]
#machines = [fill("madsmax", 10); fill("madsmen", 10); fill("madsdam", 10); fill("madszem", 10); fill("madsart", 10); fill("madsend", 10)]
#machinenames = ["madsmax", "madsmen", "madsdam", "madszem", "madsart", "madsend"]
#machinenames = ["madsmax", "madsmen", "madsdam", "madszem", "madskil", "madsart", "madsend"]
machinenames = ["madsmen", "madsdam", "madszem", "madskil", "madsart", "madsend", "madsmax"]
#machinenames = ["madsmax", "madsdam", "madszem", "madskil", "madsart", "madsend"]
#machinenames = ["madsmax", "madsmen", "madsdam", "madszem", "madskil", "madsart"]
const numprocspermachine = 32
if nprocs() == 1
	for i = 1:numprocspermachine
		addprocs(machinenames)#do it this way so that the worker id's are not blocked onto each machine to help load balance
		println("adding procs")
	end
end
@show nprocs()
@everywhere workingdir = remotecall_fetch(1, ()->pwd())
@everywhere cd(workingdir)
@everywhere println(pwd())
@show nprocs()
import Mads
@show nprocs()
if !isdefined(:md)
	#md = Mads.loadmadsfile("w01.mads")
	md = Mads.loadyamlmadsfile("w01-rerun.mads")
end
@show nprocs()
#=
g = Mads.makemadscommandgradient(md)
paramvalues = Dict(zip(Mads.getparamkeys(md), Mads.getparamsinit(md)))
g(paramvalues)
=#
#result = pmap(i->Mads.forward(md), 1:length(machinenames))
#forwardrun = Mads.forward(md)
results = Mads.calibrate(md; np_lambda=84, lambda_mu=sqrt(3.))
