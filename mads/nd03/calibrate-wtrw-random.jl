info("start")
include(joinpath(Pkg.dir("Mads"), "src-interactive/MadsParallel.jl"))
info("setprocs")
setprocs(ntasks_per_node=2)

# @everywhere ENV["MADS_NO_PYTHON"] = ""
@everywhere ENV["MADS_NO_DISPLAY"] = ""
@everywhere ENV["MADS_NO_PLOT"] = ""

info("import")
reload("Mads")
@everywhere Mads.quietoff()

info("load")
filename = Mads.getnextmadsfilename("w01-v01.mads")
info("Filename: $(filename)")
@time md = Mads.loadmadsfile(filename; bigfile=true)
delete!(md, "RestartDir")
parameters_old = Mads.getparamdict(md)
while true
	info("calibrate")
	inverse_parameters, inverse_results = Mads.calibraterandom(md, 100; np_lambda=42, lambda_mu=2., tolX=1e-12, seed=2016)
	@show ReusableFunctions.restarts
	if parameters_old == inverse_parameters
		warn("Calibration done!")
		break
	end
	Mads.setparamsinit!(md, inverse_parameters)
	filename = Mads.setnewmadsfilename(md)
	info("Filename: $(filename)")
	md["Filename"] = filename
	Mads.savemadsfile(md, filename, observations_separate=true)
end
