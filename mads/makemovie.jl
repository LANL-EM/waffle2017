import FEHM
import JLD

include("makemoviesnapshots.jl")

function makemovie(; workingdir::AbstractString=".", rootname::AbstractString="w01", jldrootname::AbstractString="wl", force::Bool=false, case::AbstractString="waffle", snapshotdir::AbstractString="snapshots", moviedir::AbstractString="movies", topnodesfile::AbstractString=joinpath(workingdir, "..", "..", "smoothgrid2", "out_top.nodes_xyz"), geofile::AbstractString=joinpath(workingdir, "..", "..", "smoothgrid2", "tet.geo"))

	info("Case: $case")

	info("Current  directory: $(pwd())")
	info("Working  directory: $workingdir")
	info("Movie    directory: $moviedir")
	info("Snapshot directory: $snapshotdir")

	if !isdir(moviedir)
		mkdir(moviedir)
	end
	if !isdir(snapshotdir)
		mkdir(snapshotdir)
	end

	if !isfile("$workingdir/$jldrootname.jld") || force==true
		# timefilter = t->t > 0 && t < (2029 - 1964) * 365.25 && abs(round(t / 365.25) - t / 365.25) < 1e-2
		info("Creating $workingdir/$jldrootname.jld ...")
		FEHM.avs2jld(geofile, "$workingdir/$jldrootname/$rootname", "$workingdir/$jldrootname.jld")
	end
	xs, ys, zs, times, crdata = JLD.load("$workingdir/$jldrootname.jld", "xs", "ys", "zs", "times", "Cr")
	topnodes = map(Int, readdlm(topnodesfile)[:, 1])
	#set up the data for the top of the water table
	topdata, thirteenmeterdata = gettop13mdata(xs, ys, zs, times, topnodes, crdata)

	x0 = CrPlots.getwelllocation("R-62")[1] - 250
	x1 = CrPlots.getwelllocation("R-36")[1] + 250
	y0 = CrPlots.getwelllocation("R-50")[2] - 500
	y1 = CrPlots.getwelllocation("R-43")[2] + 250
	boundingbox = (x0, y0, x1, y1)
	plotwells = [["CrPZ-$i" for i in 1:5]; ["CrIN-$i" for i in 1:5]; ["CrEX-$i" for i in [1, 3]]; ["R-61", "R-42", "R-28", "R-50", "R-44", "R-45", "R-13", "SIMR-2", "R-43", "R-62", "R-15", "PM-03", "O-04", "R-11", "R-36"]]
	lowerlimit = 50
	upperlimit = 1000

	for (prefix, data) in zip(["top", "t13m"], [topdata, thirteenmeterdata])
		rootfilenamemovie = "$workingdir/$moviedir/$(case)_$prefix"
		rootfilenamesnapshot = "$workingdir/$snapshotdir/$(case)_$prefix"
		makemoviesnapshots(xs, ys, times, data, topnodes, lowerlimit, upperlimit, rootfilenamesnapshot, rootfilenamemovie, boundingbox, plotwells)
	end
end

makemovie(; workingdir="cond11_scenarios", case="cond11", jldrootname="w01_no_action")
