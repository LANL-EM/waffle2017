import Interpolations
import FEHM
import JLD

include("makemoviesnapshots.jl")

function scenariomovies(scenarios::Associative; rootname::AbstractString="w01", workingdir::AbstractString=".", case::AbstractString="waffle", snapshotdir::AbstractString="scenario_movie_snapshots", moviedir::AbstractString="scenario_movies", topnodesfile::AbstractString=joinpath(workingdir, "..", "..", "smoothgrid2", "out_top.nodes_xyz"), deltaonly::Bool=false, stepsperyear::Int64=10, dataset::String="Cr")

	info("Case: $case")

	info("Scenarios:")
	display(scenarios)

	info("Current  directory: $(pwd())")
	info("Working  directory: $workingdir")
	info("Movie    directory: $moviedir")
	info("Snapshot directory: $snapshotdir")

	if !isdir(moviedir)
		mkdir(moviedir)
	end
	if !isdir(snapshotdir)
		mkdir(snapshotdir)
	end

	xs = nothing
	ys = nothing
	zs = nothing
	mddatas = Dict()
	timemin = 2005
	timemax = 2027
	timedays = collect(365.25:365.25/stepsperyear:23376.0)
	timeyear = timedays./365.25 + 1964
	if VERSION >= v"0.6"
		times = timedays[(timeyear.>timemin) .& (timeyear.<timemax)]
	else
		times = timedays[(timeyear.>timemin) & (timeyear.<timemax)]
	end
	@show times ./ 365.25 + 1964
	for scenarioname in keys(scenarios)
		if !isfile("$workingdir/$(rootname)_$(scenarioname).jld")
			warn("File $workingdir/$(rootname)_$(scenarioname).jld is missing! Skipping scenario $scenarioname ...")
		else
			info("Processing scenario $scenarioname using file $workingdir/$(rootname)_$(scenarioname).jld ...")
			xs, ys, zs, thesetimes, thismddata = JLD.load("$workingdir/$(rootname)_$(scenarioname).jld", "xs", "ys", "zs", "times", dataset)
			info("The first 5 entries will be removed ($(thesetimes[1:6]))!")
			itp = Interpolations.interpolate((thesetimes[6:end],), thismddata[6:end], Interpolations.Gridded(Interpolations.Linear())) #TODO duck tape to be fix; remove the first dummy FEHM times in the output
			mddatas[scenarioname] = map(t->itp[t], times)
		end
	end
	topnodes = map(Int, readdlm(topnodesfile)[:, 1])

	x0 = CrPlots.getwelllocation("R-62")[1] - 250
	x1 = CrPlots.getwelllocation("R-36")[1] + 250
	y0 = CrPlots.getwelllocation("R-50")[2] - 500
	y1 = CrPlots.getwelllocation("R-43")[2] + 250
	boundingbox = (x0, y0, x1, y1)
	plotwells = [["CrPZ-$i" for i in 1:5]; ["CrIN-$i" for i in 1:6]; ["CrEX-$i" for i in 1:4]; ["R-61", "R-42", "R-28", "R-50", "R-44", "R-45", "R-13", "SIMR-2", "R-43", "R-62", "R-15", "PM-03", "O-04", "R-11", "R-36"]]
	if dataset == "Cr"
		lowerlimit = 50
		upperlimit = 1000
		legendtille = "Cr [ppb]"
	else
		lowerlimit = 1774
		upperlimit = 1785
		legendtille = "WL [m]"
	end

	if !deltaonly
		for scenarioname in keys(mddatas)
			mddata = mddatas[scenarioname]
			#set up the data for the top of the water table
			topdata, thirteenmeterdata = gettop13mdata(xs, ys, zs, times, topnodes, mddata)
			for (prefix, data) in zip(["top_$(scenarioname)", "thirteenmeters_$(scenarioname)"], [topdata, thirteenmeterdata])
#			for (prefix, data) in zip(["top_$(scenarioname)"], [topdata])
				rootfilenamemovie = "$workingdir/$moviedir/$(case)_$(dataset)_$prefix"
				rootfilenamesnapshot = "$workingdir/$snapshotdir/$(case)_$(dataset)_$prefix"
				makemoviesnapshots(xs, ys, times, data, topnodes, lowerlimit, upperlimit, rootfilenamesnapshot, rootfilenamemovie, boundingbox, plotwells; infoouput="$(dataset) snapshot", legendtitle=legendtille)
			end
		end
	end

	if dataset == "Cr"
		legendtille = "δCr [ppb]"
	else
		legendtille = "δWL [m]"
	end

	for scenarioname1 in keys(mddatas), scenarioname2 in ["no_action"]
		if scenarioname1 == scenarioname2
			continue
		end
		mddata = mddatas[scenarioname1] - mddatas[scenarioname2]
		topdata, thirteenmeterdata = gettop13mdata(xs, ys, zs, times, topnodes, mddata)
		for (prefix, data) in zip(["top_$(scenarioname1)_minus_$(scenarioname2)", "t13m_$(scenarioname1)_minus_$(scenarioname2)"], [topdata, thirteenmeterdata])
#		for (prefix, data) in zip(["top_$(scenarioname1)_minus_$(scenarioname2)"], [topdata])
			diffupperlimit = maximum(map(maximum, data))
			difflowerlimit = minimum(map(minimum, data))
			rootfilenamemovie = "$workingdir/$moviedir/$(case)_$(dataset)_$prefix"
			rootfilenamesnapshot = "$workingdir/$snapshotdir/$(case)_$(dataset)_$prefix"
			makemoviesnapshots(xs, ys, times, data, topnodes, difflowerlimit, diffupperlimit, 0, rootfilenamesnapshot, rootfilenamemovie, boundingbox, plotwells; infoouput="Delta-$(dataset) snapshot", legendtitle=legendtille)
		end
	end
end
