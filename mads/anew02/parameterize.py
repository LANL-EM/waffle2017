#!/usr/bin/env python
import aquiferdb as db
import numpy as np
import madspy

db.connecttodb()

heights = [1700.,             1700.,             1700.,             1700.,            1700.,             1700.,            1700.,            1700.,            1700.,            1700.,            1700.,
		1774.,              1774.,              1774.,              1774.,              1774.,              1774.,
		1774.,            1774.,            1774.,
		1774.,                     1774.,
		1774.,            1774.,
		1700.,             1700.,
		1774.,             1774.,
		1774.,             1774.,           1774.,
		1774.,
		1700.,                    1700.,                    1700.,
		1774.,                    1774.,                    1774.,
		1774.,
		1774.,
                1774., 1700., 1774., 1774., 1700.,
				1760.]
bisectors = [["PM-05", "R-15"], ["PM-04", "R-61"], ["R-37", "R-35b"], ["R-13", "R-36"], ["R-45", "PM-03"], ["R-28", "R-42"], ["R-15", "R-15"], ["R-36", "R-36"], ["R-61", "R-61"], ["R-62", "R-62"], ["R-11", "R-11"],
		["CrEX-1", "R-50"], ["CrEX-1", "R-44"], ["CrEX-1", "R-45"], ["CrEX-1", "R-28"], ["CrEX-1", "R-42"], ["CrEX-1", "R-61"],#these are the upper layer pilot points for crex1_hhh_s_multilayer2
		["R-28", "R-45"], ["R-28", "R-11"], ["R-28", "R-42"],
		["R-42", "R-43", "R-62"], ["R-42", "R-62", "R-15", "R-61"],
		["R-62", "R-01"], ["R-15", "R-33"],
		["PM-02", "PM-02"], ["PM-04", "PM-04"],
		["PM-02", "PM-02"], ["PM-04", "PM-04"],
		["R-11", "R-42"], ["R-11", "R-43"], ["R-01", "R-62", "O-04"],
		["R-01"],
		["PM-04", "PM-04", "R-15"], ["PM-04", "R-15", "R-15"], ["PM-04", "R-44"],
		["PM-04", "PM-04", "R-15"], ["PM-04", "R-15", "R-15"], ["PM-04", "R-44"],
		["R-15"],
		["R-33"],
                ["R-35b"], ["R-35b"], ["R-62"], ["R-43"], ["R-43"],
				["R-45"]]
wells = sum(bisectors, [])#this is a trick for turning the bisectors into a flat list
wellinfo = {}
for well in wells:
	print(well)
	wellinfo[well] = np.array(db.getgeometry(well)[0:2])

db.disconnectfromdb()

def getmidpoint(somewells):
	wellinfos = np.array(map(lambda x: wellinfo[x], somewells))
	midpoint = np.mean(wellinfos, 0)
	#midpoint = .5 * (wellinfo[twowells[0]] + wellinfo[twowells[1]])
	return list(midpoint)

pilotpoints = map(getmidpoint, bisectors)
fpp = open("pilotpoints.txt", "w")
fhyco = open("w01.tpl_hyco_hetero", "w")
fhyco.write("template #\n")
fstor = open("w01.tpl_stor_hetero", "w")
fstor.write("template #\n")
for i in range(0, len(pilotpoints) - 1):
	fpp.write(str(pilotpoints[i][0]) + " " + str(pilotpoints[i][1]) + " " + str(heights[i]) + "\n")
	fhyco.write("#kxy" + str(i + 1) + "# ")
	fstor.write("#s" + str(i + 1) + "# ")
fpp.write(str(pilotpoints[len(pilotpoints) - 1][0]) + " " + str(pilotpoints[len(pilotpoints) - 1][1]) + " " + str(heights[len(pilotpoints) - 1]))
fpp.close()
fhyco.write("#kxy" + str(len(pilotpoints)) + "#")
fstor.write("#s" + str(len(pilotpoints)) + "#")
fstor.close()
for i in range(0, len(pilotpoints)):
	fhyco.write(" #kz" + str(i + 1) + "#")
fhyco.close()
for i in range(0, len(pilotpoints)):
	print("- kxy" + str(i + 1) + ": { longname \"kxy" + str(i + 1) + "\", init: -4.7, type: opt, log: no, min: -8, max: -2 }")
	print("- kz" + str(i + 1) + ": { longname \"kz" + str(i + 1) + "\", init: -6, type: opt, log: no, min: -9, max: -3 }")
	print("- s" + str(i + 1) + ": { longname \"s" + str(i + 1) + "\", init: -3, type: opt, log: no, min: -8, max: -1 }")
