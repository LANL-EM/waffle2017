import RobustPmap
info("start runmodel.jl")
include("runmodelcode.jl")
info("setup")
setupmaindir()
info("pre")
RobustPmap.rpmap(preprocess, dirs)
info("run")
RobustPmap.rpmap(runmodelindir, dirs)
info("post")
RobustPmap.rpmap(postprocess, dirs)
