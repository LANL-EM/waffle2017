import yaml

"""
f = open("w01.mads")
data = yaml.load(f)
f.close()
"""

numconcobs = 0
sumnewweights = 0.
r61makeweightnonzero = {}
for obs in data["Observations"]:
    obsname = obs.keys()[0]
    if "P" in obsname and "r61" in obsname:
        otherobsname = "_".join(obsname.split("_")[0:-1])
        realobs = obs[obsname]
        if realobs["target"] != 0:
            r61makeweightnonzero[otherobsname] = True

for obs in data["Observations"]:
    obsname = obs.keys()[0]
    if "P" in obsname and "r61" in obsname:
        pwells = ["pm01", "pm02", "pm03", "pm04", "pm05", "o04"]
        otherobsname = "_".join(obsname.split("_")[0:-1])
        realobs = obs[obsname]
        if otherobsname in r61makeweightnonzero:
            realobs["weight"] = 0
            for pwell in pwells:
                if pwell in obsname:
                    realobs["weight"] = 10
"""
    if "P" in obsname and "r15" in obsname:
        realobs = obs[obsname]
        if realobs["weight"] > 0:
            realobs["weight"] = 1
    if "P" in obsname and "r28_crex1" in obsname:
        realobs = obs[obsname]
        if realobs["weight"] > 0:
            realobs["weight"] = 0
            """
"""
for obs in data["Observations"]:
    obsname = obs.keys()[0]
    if "conc_" in obsname:
        numconcobs += 1
        realobs = obs[obsname]
        newweight = 1. / max(20., realobs["target"])
        sumnewweights += newweight
for obs in data["Observations"]:
    obsname = obs.keys()[0]
    if "conc_" in obsname:
        realobs = obs[obsname]
        newweight = 1. / max(20., realobs["target"])
        realobs["weight"] = numconcobs / sumnewweights * newweight
"""
f = open("w01_new.mads", "w")
yaml.dump(data, f, width=255)
f.close()
