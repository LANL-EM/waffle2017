import FEHM

function runscenario(scenarioname, wellsinuse, pntstartyear_in, gpmrates, pumptimes_in; force::Bool=false)
	gpm2kgpersec = 0.06309
	kgpersecrates = map(x->x * gpm2kgpersec, gpmrates)
	pntstartyear = FEHM.year2fehmday(pntstartyear_in; offset=modelstartyear)
	pumptimes = FEHM.year2fehmday(pumptimes_in; offset=modelstartyear)

	info("Run scenario $scenarioname")
	if !isdir("wl_$scenarioname") || force==true
		initialdir = pwd()
		cp("wl", "wl_$scenarioname"; remove_destination=true)
		cd("wl_$scenarioname")
		info("Set up FEHM $scenarioname")
		rm("w01.data")
		datalines = FEHM.myreadlines("../wl.data")
		f = open("w01.data", "w")
		write(f, datalines[1])
		write(f, "cont\navs 1 1e20\nconcentrations\nliquid\nhead\nendavs\n")
		i = 2
		while datalines[i] != "time\n"
			write(f, datalines[i])
			i += 1
		end
		write(f, datalines[i]) # write "time\n"
		i += 1
		write(f, datalines[i]) # write group 1 of time macro
		i += 1
		dittimes = convert(Vector{Float64}, vec(unique(vcat(scenarios[scenarioname]["pumptimes"]...))))
		ditsteps = map(x->365.25, dittimes)
		while datalines[i] != "\n" # write dit groups of time macro
			splitline = split(datalines[i])
			currentday = parse(Float64, splitline[1])
			if currentday + 1e-2 >= pntstartyear
				for j = 0:19
					if !(currentday + .5 * 36.525 * j in dittimes)
						push!(dittimes, currentday + .5 * 36.525 * j)
						push!(ditsteps, .5 * 36.525)
					end
				end
			else
				if !(parse(Float64, split(datalines[i])[1]) in dittimes)
					push!(dittimes, parse(Float64, split(datalines[i])[1]))
					push!(ditsteps, parse(Float64, split(datalines[i])[end]))
				end
			end
			i += 1
		end
		ditsortedindices = sort(collect(1:length(dittimes)), by=i->dittimes[i])
		for j = 1:length(dittimes)
			write(f, "$(dittimes[ditsortedindices[j]]) -1 1 1 $(ditsteps[ditsortedindices[j]])\n")
		end
		for j = i:length(datalines)
			write(f, datalines[j])
		end
		close(f)
		rm("w01.boun")
		bounlines = FEHM.myreadlines("../wl.boun")
		bounendline = filter(i->bounlines[i] == "end\n", 1:length(bounlines))[1]
		f = open("w01.boun", "w")
		for i = 1:bounendline - 1
			if !contains(bounlines[i], "year")
				write(f, bounlines[i])
			end
		end
		for i = 1:length(wellsinuse)
			write(f, "model $(i + 2) $(wellsinuse[i])\n")
			write(f, "tran\ntime\n$(1 + length(gpmrates[i]))\n")
			write(f, "0\n")
			for j = 1:length(pumptimes[i])
				write(f, "$(pumptimes[i][j])\n")
			end
			write(f, "dsw\n0\n")
			for j = 1:length(kgpersecrates[i])
				write(f, "$(kgpersecrates[i][j])\n")
			end
		end
		write(f, "end\n")
		for i = 1:length(wellsinuse)
			write(f, "-$(names2zones[wellsinuse[i]]) 0 0 $(i + 2)\n")
		end
		for i = bounendline+1:length(bounlines)
			write(f, bounlines[i])
		end
		close(f)
		rm("wl.trac")
		traclines = FEHM.myreadlines("../wl.trac")
		f = open("wl.trac", "w")
		write(f, traclines[1])
		write(f, "0.0 1.1 1.0E-3 1.\n")
		for i = 3:length(traclines) - 1
			write(f, traclines[i])
		end
		for i = 1:length(wellsinuse)
			if minimum(gpmrates[i]) < 0
				write(f, "-$(names2zones[wellsinuse[i]]) 0 0 5 $(minimum(pumptimes[i])) $(maximum(pumptimes[i]))\n")
			end
		end
		write(f, "\n")
		close(f)
		info("Run FEHM ...")
		@time run(pipeline(`xfehm w01.files`, stdout=DevNull, stderr=DevNull))
		run(`ln -s $geofilename w01.00001_geo`)
		cd(initialdir)
	else
		println("Skip running $scenarioname")
	end
end
