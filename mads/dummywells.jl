import FEHM
import DataStructures

function dummywells(; centerwell::AbstractString="CrIN6", centerzone::Integer=610601, centerx::Number=499950.3031, centery::Float64=539101.3053, zonefile::AbstractString="", radius::Vector=[60, 120, 240, 480], verticaldisplacement::Vector=[40, 80, 120, 160], xyznodesfile::AbstractString=joinpath("..", "..", "smoothgrid2", "tet.xyz"))

	wellnames = Array{String}(0)
	zonenumbers = Array{Int64}(0)
	nodenumbers = Array{Vector{Int64}}(0)
	coordinates = Array{Vector{Float64}}(0)

	xyznodes = readdlm(xyznodesfile)
	n, d = FEHM.getwellnodes(xyznodes, centerx, centery)
	push!(wellnames, centerwell)
	push!(zonenumbers, centerzone)
	push!(nodenumbers, n)
	push!(coordinates, [centerx, centery])
	info("$centerwell nodes $(n) lateral disparity $(d)")

	i = 1
	for (rf, vf) = zip([1, 1, 1, -1, -1, -1], [1, 0, -1, 1, 0, -1])
		for (r, v) = zip(radius, verticaldisplacement)
			h = sqrt(r^2 - v^2) * rf
			v *= vf
			n, d = FEHM.getwellnodes(xyznodes, centerx-h, centery-v)
			wd = rf == 1 ? "e" : "w"
			nd = vf == 1 ? "n" : (vf == 0 ? "c" : "s")
			wn = centerwell * wd * nd * "$r"
			zn = centerzone + i
			# info("$wn - $i: radius $r vert $v hor $h nodes $n lateral disparity $d")
			push!(wellnames, wn)
			push!(zonenumbers, zn)
			push!(nodenumbers, n)
			push!(coordinates, [centerx-h, centery-v])
			i += 1
		end
	end
	@assert length(unique(vcat(nodenumbers...))) == length(zonenumbers) * 2
	if zonefile != ""
		FEHM.dumpzone(zonefile, zonenumbers, nodenumbers)
	end
	return wellnames, zonenumbers, nodenumbers, coordinates
end