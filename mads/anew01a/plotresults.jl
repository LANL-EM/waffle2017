import Mads
import Gadfly
import DataStructures
import DataFrames
import PyCall
@PyCall.pyimport aquiferdb as db
db.connecttodb()

if !isdefined(:md)
	@time md = Mads.loadmadsfile("w01-rerun.mads")
end
if !isdefined(:result)
	result = Mads.forward(md)
end
k2t(s)=float(split(s, "_")[end])
k2well(s)=join(split(s, "_")[2:end], " #")
xlday2date(i)=Date(1899, 12, 30) + i * (Date(1899, 12, 30) - Date(1899, 12, 29))
k2date(s)=xlday2date(parse(Int, split(s, "_")[1][2:end]))
wellname(well) = replace(split(well, "_")[1], "r", "R-")
portdesc(well) = length(split(well, "_")) == 1 ? "SINGLE COMPLETION" : split(well, "_")[2]

obswells = ["r11", "r13", "r15", "r28", "r33_1", "r33_2", "r35b", "r36", "r42", "r43_1", "r43_2", "r44_1", "r44_2", "r45_1", "r45_2", "r50_1", "r50_2", "r61_1", "r61_2", "r62"]
pumpingwells = ["pm01", "pm02", "pm03", "pm04", "pm05", "o04", "r28", "r42", "crex1"]
Mads.plotmatches(md, result, Regex("wl_"); filename="figs/wls.pdf", format="PDF", key2time=k2well, ylabel="Water Level [m]", title="Water levels", hsize=12Gadfly.inch)
for well in obswells
	Mads.plotmatches(md, result, Regex("conc_$(well)_[0-9]+"); filename="figs/conc_$well", format="PDF", key2time=k2t, ylabel="Chromium concentration [ppb]", title=well)
	for pwell in pumpingwells
		try
			Mads.plotmatches(md, result, Regex("P[0-9]+_$(well)_$(pwell)"); filename="figs/dd_$(well)_$(pwell).pdf", format="PDF", key2time=k2date, ylabel="drawdown [m]", title="Drawdown at $well from $pwell")
		catch
			warn("plotting error for $well/$pwell")
		end
	end
end
#plot the concentrations including all the observations
for well in obswells
	concobs, obstimes = db.getchromiumconcentrations(wellname(well), portdesc(well))
	obsnames = collect(filter(x->ismatch(Regex("conc_$(well)_[0-9]+"), x), keys(md["Observations"])))
	concpreds = Float64[]
	predtimes = Any[]
	for obsname in obsnames
		push!(concpreds, result[obsname])
		push!(predtimes, DateTime(k2t(obsname)))
	end
	df = DataFrames.DataFrame(time=[predtimes; obstimes], concentration=[concpreds; concobs], obsorpred=[fill("pred", length(concpreds)); fill("obs", length(concobs))])
	if length(concpreds) > 0
		#=
		p = Gadfly.plot(df, x="time", y="concentration", color="obsorpred", Gadfly.Guide.yticks(ticks=collect(0:200:1400)), Gadfly.Guide.xticks(ticks=map(DateTime, collect(2000:4:2016))), Gadfly.Guide.title("$well"), Gadfly.Guide.ylabel("Concentration [ppb]"), Gadfly.Theme(highlight_width=0Gadfly.pt))
		Gadfly.draw(Gadfly.PDF("figs/totalconc1_$(well).pdf", 8Gadfly.inch, 6Gadfly.inch), p)
		p = Gadfly.plot(df, x="time", y="concentration", color="obsorpred", Gadfly.Guide.xticks(ticks=map(DateTime, collect(2000:4:2016))), Gadfly.Guide.title("$well"), Gadfly.Guide.ylabel("Concentration [ppb]"), Gadfly.Theme(highlight_width=0Gadfly.pt))
		Gadfly.draw(Gadfly.PDF("figs/totalconc2_$(well).pdf", 8Gadfly.inch, 6Gadfly.inch), p)
		p = Gadfly.plot(df, x="time", y="concentration", color="obsorpred", Gadfly.Guide.title("$well"), Gadfly.Guide.ylabel("Concentration [ppb]"), Gadfly.Theme(highlight_width=0Gadfly.pt))
		Gadfly.draw(Gadfly.PDF("figs/totalconc_$(well).pdf", 8Gadfly.inch, 6Gadfly.inch), p)
		=#
		p = Gadfly.plot(df, x="time", y="concentration", color="obsorpred", Gadfly.Guide.xticks(ticks=map(DateTime, collect(2000:4:2016))), Gadfly.Guide.title("$well"), Gadfly.Guide.ylabel("Concentration [ppb]"), Gadfly.Theme(highlight_width=0Gadfly.pt))
		Gadfly.draw(Gadfly.PDF("figs/totalconc_$(well).pdf", 8Gadfly.inch, 6Gadfly.inch), p)
	else
		warn("no drawdowns for $well")
	end
end
#plot the drawdown for all the pumping wells
for owell in obswells
	totaldrawdown = DataStructures.DefaultDict(0.)
	totaldrawdowntarget = DataStructures.DefaultDict(0.)
	obsnames = collect(filter(x->ismatch(Regex("P\\d+_$(owell)_"), x), keys(md["Observations"])))
	obsnames = collect(filter(x->!contains(x, "crex1"), obsnames))
	for obsname in obsnames
		if md["Observations"][obsname]["weight"] > 0 || owell == "r61_1" || owell == "r61_2"
			totaldrawdown[k2date(obsname)] += result[obsname]
			totaldrawdowntarget[k2date(obsname)] += md["Observations"][obsname]["target"]
		end
	end
	if length(totaldrawdown) > 0
		df = DataFrames.DataFrame(time=[collect(keys(totaldrawdown)); collect(keys(totaldrawdowntarget))], drawdown=[collect(values(totaldrawdown)); collect(values(totaldrawdowntarget))], obsorpred=[fill("pred", length(totaldrawdown)); fill("obs", length(totaldrawdowntarget))])
		p = Gadfly.plot(df, x="time", y="drawdown", color="obsorpred", Gadfly.Guide.title("$owell"), Gadfly.Guide.ylabel("Drawdown [m]"), Gadfly.Theme(highlight_width=0Gadfly.pt))
		Gadfly.draw(Gadfly.PDF("figs/totaldd_$(owell).pdf", 8Gadfly.inch, 6Gadfly.inch), p)
	else
		warn("no drawdowns for $owell")
	end
end
for owell in obswells
	println(string("OF from concs at $owell (points, trend): ", Mads.partialof(md, result, Regex("conc_$(owell)_[0-9]+")), ", ", Mads.partialof(md, result, Regex("conc_$(owell)_slope"))))
end
for pwell in pumpingwells
	println(string("OF from pumping at $pwell: ", Mads.partialof(md, result, Regex("P.*_$pwell"))))
end
println(string("OF from wls: ", Mads.partialof(md, result, Regex("wl_"))))
println(string("OF from concentration trends: ", Mads.partialof(md, result, Regex("_slope"))))
db.disconnectfromdb()
