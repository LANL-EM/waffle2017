import JLD
import Mads

#machinenames = ["es$(lpad(i, 2, 0))" for i in 7:27 if i != 24 && i != 8 && i != 22]
#machinenames = ["es$(lpad(i, 2, 0))" for i in 7:27 if !(i in [8, 17, 19, 20, 21, 22, 23, 24])]
#machinenames = ["es$(lpad(i, 2, 0))" for i in 7:27 if !(i in [24])]
machinenames = ["es08", "es10", "es11", "es16", "es20", "es21", "es22", "es25", "es26", "es27"]

include(joinpath(Pkg.dir("Mads"), "src-interactive/MadsParallel.jl"))

info("Set 16 parallel tasks on each of $(machinenames)")
setprocs(nodenames=machinenames, ntasks_per_node=16, quiet=false)

@show nprocs()
@eval @everywhere workingdir = $(pwd())
@everywhere cd(workingdir)
@everywhere println(pwd())
@show nprocs()

if !isdefined(:md)
	md = Mads.loadmadsfile("w01-v03.mads")
end

chain = Mads.emceesampling(md; numwalkers=2 * nworkers(), burnin=10 * nworkers(), nsteps=100 * nworkers(), seed=1234, weightfactor=1e-4, sigma=1e-4)
JLD.save("chain.jld", "chain", chain)
