import RobustPmap
import FEHM

FEHM.checknode()
info("Runmodel start ...")
include("runmodelcode.jl")
info("Runmodel setup ...")
setupmaindir()
info("Runmodel preprocessing ...")
FEHM.checknode()
RobustPmap.rpmap(preprocess, dirs)
info("Runmodel executing ...")
FEHM.checknode()
RobustPmap.rpmap(FEHM.runmodelindir, dirs)
info("Runmodel postprocessing ...")
FEHM.checknode()
RobustPmap.rpmap(postprocess, dirs)
info("Runmodel done!")

:done
